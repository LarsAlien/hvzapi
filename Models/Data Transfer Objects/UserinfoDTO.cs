namespace HVZapi.Models.Data_Transfer_Objects
{
  public class UserinfoDTO
    {
        public int Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Username { get; set; }
        public string Email { get; set; }
        public string Age { get; set; }
        public string Phone { get; set; }
    }
}