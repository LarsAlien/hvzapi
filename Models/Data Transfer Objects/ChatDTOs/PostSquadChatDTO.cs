﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace HVZapi.Models.Data_Transfer_Objects
{
    public class PostSquadChatDTO
    {
        [RegularExpression(@"^[.a-zA-Z0-9,!?;:'\- ]*$",
    ErrorMessage = "You appear to be using characters which are not permitted")]
        [DataType(DataType.Text)]
        public string Message { get; set; }

        public int SquadId { get; set; }
        public int PlayerId { get; set; }
    }
}
