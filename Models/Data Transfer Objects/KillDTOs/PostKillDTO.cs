﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace HVZapi.Models.Data_Transfer_Objects
{
    public class PostKillDTO
    {
        public DateTime TOD { get; set; }

        public bool Is_Visible { get; set; }

        [StringLength(150)]
        [RegularExpression(@"^[.a-zA-Z0-9,!?;:'\- ]*$",
    ErrorMessage = "You appear to be using characters which are not permitted")]
        public string Story { get; set; }

        public decimal lat { get; set; }

        public decimal lng { get; set; }

        public int GameId { get; set; }
        public int KillerId { get; set; }
        public int VictimId { get; set; }
    }
}
