﻿using System;
using System.ComponentModel.DataAnnotations;

namespace HVZapi.Models.Data_Transfer_Objects
{
    public class KillPlayerDTO
    {
        [Required]
        public DateTime TOD { get; set; }

        [Required]
        public bool IsVisible { get; set; }

        public string Story { get; set; }

        [Required]
        public decimal lat { get; set; }

        [Required]
        public decimal lng { get; set; }

        [Required]
        public int GameId { get; set; }
        [Required]
        public int KillerId { get; set; }
        [Required]
        public string BiteCode { get; set; }
    }
}
