﻿using System;



namespace HVZapi.Models.Data_Transfer_Objects
{
    public class GetKillDTO
    {
        public int Id { get; set; }
        public DateTime TOD { get; set; }

        public bool IsVisible { get; set; }

        public string Story { get; set; }

        public decimal lat { get; set; }

        public decimal lng { get; set; }

        public int GameId { get; set; }
        public int KillerId { get; set; }
        public int VictimId { get; set; }
    }
}
