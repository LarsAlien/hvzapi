﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HVZapi.Models.Data_Transfer_Objects
{
    public class JoinGameDTO
    {
        public int GameId { get; set; }
        public int UserId { get; set; }
    }
}
