﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HVZapi.Models.Data_Transfer_Objects
{
    public class GetGameDTO
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public bool GameStarted { get; set; }
        public bool RegistrationOpen { get; set; }
        public bool GameComplete { get; set; }
        public string ImageUrl { get; set; }
        public DateTime StartTime { get; set; }
        public DateTime EndTime { get; set; }
        public decimal NW_lat { get; set; }
        public decimal NW_lng { get; set; }
        public decimal SE_lat { get; set; }
        public decimal SE_lng { get; set; }
        public int CountPlayers { get; set; }
        public List<GetPlayerDTO> Players { get; set; }
    }
}
