﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace HVZapi.Models.Data_Transfer_Objects
{
    public class PutGameDTO
    {
        public int Id { get; set; }
        [RegularExpression(@"^[.a-zA-Z0-9,!?;:'\- ]*$",
    ErrorMessage = "You appear to be using characters which are not permitted")]
        [DataType(DataType.Text)]
        public string Name { get; set; }
        public bool RegistrationOpen { get; set; }
        public bool GameStarted { get; set; }
        public bool GameComplete { get; set; }
        public string ImageUrl { get; set; }
        public DateTime StartTime { get; set; }
        public DateTime EndTime { get; set; }
        public decimal NW_lat { get; set; }
        public decimal NW_lng { get; set; }
        public decimal SE_lat { get; set; }
        public decimal SE_lng { get; set; }
    }
}
