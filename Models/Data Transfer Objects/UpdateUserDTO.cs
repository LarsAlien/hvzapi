using System.ComponentModel.DataAnnotations;

namespace WebApi.Models.Users
{
  public class UpdateUserDTO
    {
        [RegularExpression(@"^[a-zA-Z'\-\s0-9]+$",
 ErrorMessage = "Characters are not allowed.")]
        [DataType(DataType.Text)]
        [Display(Order = 1, Name = "First Name")]
        public string FirstName { get; set; }

        [RegularExpression(@"^[a-zA-Z'\-\s0-9]+$",
 ErrorMessage = "Characters are not allowed.")]
        [DataType(DataType.Text)]
        [Display(Order = 2, Name = "Last Name")]
        public string LastName { get; set; }

        [RegularExpression(@"^[a-zA-Z'\-\s0-9]+$",
 ErrorMessage = "Characters are not allowed.")]
        [DataType(DataType.Text)]
        [Display(Order = 1, Name = "Last Name")]
        public string Username { get; set; }

        [DataType(DataType.EmailAddress)]
        public string Email { get; set; }
        public int Age { get; set; }
        [DataType(DataType.PhoneNumber)]
        public string Phone { get; set; }


        [DataType(DataType.Password)]
        public string Password { get; set; }
    }
}




