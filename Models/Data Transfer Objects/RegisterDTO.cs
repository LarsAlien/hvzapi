using System.ComponentModel.DataAnnotations;

namespace WebApi.Models.Users
{
    public class RegisterDTO
    {
        [RegularExpression(@"^[a-zA-Z'\-\s0-9]+$",
ErrorMessage = "Characters are not allowed.")]
        [DataType(DataType.Text)]
        [Display(Order = 1, Name = "First Name")]
        [Required]
        public string FirstName { get; set; }

        [RegularExpression(@"^[a-zA-Z'\-\s0-9]+$",
 ErrorMessage = "Characters are not allowed.")]
        [DataType(DataType.Text)]
        [Display(Order = 2, Name = "Last Name")]
        [Required]
        public string LastName { get; set; }

        [RegularExpression(@"^[a-zA-Z'\-\s0-9]+$",
 ErrorMessage = "Characters are not allowed.")]
        [DataType(DataType.Text)]
        [Display(Order = 3, Name = "Username")]
        [Required]
        public string Username { get; set; }

        [Required]
        [StringLength(50, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 8)]
        [DataType(DataType.Password)]
        [RegularExpression(@"(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[@$!%*?&])[A-Za-z\d@$!%*?&]{8,}",
            ErrorMessage = "Should have at least one lower case,one upper case,one number,one special character, and Minimum 8 characters")]
        [Display(Name = "Password")]


        public string Password { get; set; }
        [Required]
        public int Age { get; set; }
        [DataType(DataType.PhoneNumber)]
        [Required]
        public string Phone { get; set; }

        [Required]
        public string Role { get; set; }

        [Required]
        public string Email { get; set; }
    }
}