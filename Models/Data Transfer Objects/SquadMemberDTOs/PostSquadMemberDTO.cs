﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace HVZapi.Models.Data_Transfer_Objects
{
    public class PostSquadMemberDTO
    {
        public int GameId { get; set; }
        public int SquadId { get; set; }
        public int PlayerId { get; set; }
    }
}
