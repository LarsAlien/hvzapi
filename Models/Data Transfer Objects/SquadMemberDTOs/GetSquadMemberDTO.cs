﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HVZapi.Models.Data_Transfer_Objects
{
    public class GetSquadMemberDTO
    {
        public int Id { get; set; }
        public string Rank { get; set; }
        public string Name { get; set; }
        public int GameId { get; set; }
        public int SquadId { get; set; }
        public int PlayerId { get; set; }
        public bool IsHuman { get; set; }
    }
}
