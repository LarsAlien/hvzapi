﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HVZapi.Models.Data_Transfer_Objects
{
    public class PostCheckinDTO
    {
        public DateTime StartTime { get; set; }
        public DateTime EndTime { get; set; }

        // Coordinates
        public decimal Lat { get; set; }

        public decimal Lng { get; set; }


        // Foreign keys
        public int GameId { get; set; }
        public int SquadId { get; set; }
        public int SquadMemberId { get; set; }
    }
}
