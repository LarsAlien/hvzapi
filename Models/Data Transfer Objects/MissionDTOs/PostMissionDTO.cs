﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace HVZapi.Models.Data_Transfer_Objects
{
    public class PostMissionDTO
    {
        [RegularExpression(@"^[.a-zA-Z0-9,!?;:'\- ]*$",
    ErrorMessage = "You appear to be using characters which are not permitted")]
        [DataType(DataType.Text)]
        public string Name { get; set; }
        public bool IsHumanVisible { get; set; }
        public bool IsZombieVisible { get; set; }
        public string Description { get; set; }
        public DateTime StartTime { get; set; }
        public DateTime EndTime { get; set; }
        public decimal Lat { get; set; }
        public decimal Lng { get; set; }
        public int GameId { get; set; }
        public bool IsComplete { get; set; }
    }
}
