﻿using HVZapi.Models.Data_Transfer_Objects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HVZapi.Models.Data_Transfer_Objects
{
    public class GetFullSquadInfo
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public bool IsHuman { get; set; }
        public int GameId { get; set; }
        public ICollection<GetSquadMemberDTO> SquadMembers { get; set; }
        public ICollection<GetCheckinDTO> SquadCheckins { get; set; }
    }
}
