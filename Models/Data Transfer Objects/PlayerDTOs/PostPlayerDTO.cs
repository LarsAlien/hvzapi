﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HVZapi.Models.Data_Transfer_Objects
{
    public class PostPlayerDTO
    {
        public bool IsHuman { get; set; }
        public bool IsPatientZero { get; set; }
        public int UserId { get; set; }
        public int GameId { get; set; }
    }
}
