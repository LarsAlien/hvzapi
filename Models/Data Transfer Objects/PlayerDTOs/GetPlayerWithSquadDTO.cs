﻿using HVZapi.Models.Domain_Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HVZapi.Models.Data_Transfer_Objects
{
    public class GetPlayerWithSquadDTO
    {
        public int Id { get; set; }
        public bool IsHuman { get; set; }
        public bool IsPatientZero { get; set; }
        public string BiteCode { get; set; }
        public string Name { get; set; }
        public int UserId { get; set; }
        public int GameId { get; set; }
        public string SquadName { get; set; }
        public GetFullSquadInfo Squad { get; set; }
    }
}
