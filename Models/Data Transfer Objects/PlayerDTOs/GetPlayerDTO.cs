﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace HVZapi.Models.Data_Transfer_Objects
{
    public class GetPlayerDTO
    {
        public int Id { get; set; }
        public bool IsHuman { get; set; }

        [JsonIgnore]
        public bool IsPatientZero { get; set; }
        public string BiteCode { get; set; }
        public string Name { get; set; }
        public int UserId { get; set; }
        public int GameId { get; set; }
    }
}
