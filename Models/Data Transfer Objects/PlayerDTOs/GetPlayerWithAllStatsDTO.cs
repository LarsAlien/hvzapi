﻿using HVZapi.Models.Data_Transfer_Objects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HVZapi.Models.Data_Transfer_Objects
{
    public class GetPlayerWithAllStatsDTO
    {
        public int Id { get; set; }
        public bool IsHuman { get; set; }
        public bool IsPatientZero { get; set; }
        public string BiteCode { get; set; }
        public string Name { get; set; }
        public int UserId { get; set; }
        public int GameId { get; set; }
        public GetSquadDTO Squad { get;set; }
        public GetSquadMemberDTO SquadMember { get; set; }
        public GetGameDTO Game { get; set; }
        public List<GetKillDTO> KillerKills { get; set; }
        public List<GetKillDTO> VictimKills { get; set; }
    }
}
