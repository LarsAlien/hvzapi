﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace HVZapi.Models.Domain_Model
{
    /// <summary>
    /// The  Player class.
    ///  Player entity
    /// </summary>
    public class Player
    {
        public int Id { get; set; }

        [Required]
        public bool IsHuman {get; set;}
        [Required]
        public bool IsPatientZero { get; set; }
        [Required]
        public string BiteCode { get; set; }

        //Relationships
        [Required]
        [ForeignKey("UserId")]
        public int UserId { get; set; }
        public User User { get; set; }

        public SquadMember SquadMember { get; set; }

        [ForeignKey("GameId")]
        public int GameId { get; set; }
        public Game Game { get; set; }
        public  ICollection<Kill> KillerKills { get; set; }
        public   ICollection<Kill> VictimKills { get; set; }
        public ICollection<Chat> Chats { get; set; }
    }
}
