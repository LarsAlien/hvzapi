﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace HVZapi.Models.Domain_Model
{
    /// <summary>
    /// The Kill class.
    /// Kill entity
    /// </summary>
    public class Kill
    {
        public int Id { get; set; }

        [Required]
        [DataType(DataType.DateTime)]
        public DateTime TOD { get; set; }

        [Required]
        public bool IsVisible { get; set; }

        public string Story { get; set; }

        [Column(TypeName = "decimal(17,15)")]
        public decimal Lat { get; set; }

        [Column(TypeName = "decimal(18,15)")]
        public decimal Lng { get; set; }


        public int GameId { get; set; }
        [ForeignKey("GameId")]
        [Required]
        public Game Game { get; set; }

        public int KillerID { get; set; }
        [Required]
        [ForeignKey("KillerID")]
        public virtual Player Killer {get;set;}

        public int VictimID { get; set; }
         [Required]
        [ForeignKey("VictimID")]
        public virtual  Player Victim {get;set;}
    }
}

