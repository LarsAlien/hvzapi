﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace HVZapi.Models.Domain_Model
{
    /// <summary>
    /// The SquadCheckIn class.
    ///  SquadCheckIn entity
    /// </summary>
    public class SquadCheckIn
    {
        public int Id { get; set; }

        // Time
        [Required]
        [DataType(DataType.DateTime)]
        public DateTime StartTime { get; set; }
        [Required]
        [DataType(DataType.DateTime)]
        public DateTime EndTime { get; set; }

        // Coordinates
        [Required]
        [Column(TypeName = "decimal(17,15)")]
        public decimal lat { get; set; }

        [Required]
        [Column(TypeName = "decimal(18,15)")]
        public decimal lng { get; set; }


        // Foreign keys
        [Required]
        [ForeignKey("GameId")]
        public int GameId { get; set; }
        public Game Game { get; set; }

        [Required]
        [ForeignKey("SquadId")]
        public int SquadId { get; set; }
        public Squad Squad { get; set; }

        [Required]
        [ForeignKey("SquadMemberId")]
        public int SquadMemberId { get; set; }
        public SquadMember SquadMember { get; set; }
    }
}