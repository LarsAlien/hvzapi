﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace HVZapi.Models.Domain_Model
{
    /// <summary>
    /// The Squad class.
    ///  Squad entity
    /// </summary>
    public class Squad
    {
        public int Id { get; set; }

        [Required]
        [StringLength(100)]
        public string Name { get; set; }
        [Required]
        public bool IsHuman { get; set; }

        [ForeignKey("GameId")]
        public int GameId { get; set; }
        public Game Game { get; set; }

        public ICollection<SquadCheckIn> SquadCheckIns { get; set; }
        public ICollection<Chat> Chats { get; set; }
        public ICollection<SquadMember> SquadMembers { get; set; }

    }
}
