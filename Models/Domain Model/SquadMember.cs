﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace HVZapi.Models.Domain_Model
{
    /// <summary>
    /// The SquadMember class.
    ///  SquadMember entity
    /// </summary>
    public class SquadMember
    {
        public int Id { get; set; }
        [Required]
        [StringLength(30)]

        public string Rank { get; set; }

        [Required]
        [ForeignKey("GameId")]
        public int GameId { get; set; }
        public Game Game { get; set; }

        [Required]
        public int PlayerId { get; set; }
        [ForeignKey("PlayerId")]
        public Player Player { get; set; }

        [ForeignKey("SquadId")]
        public int? SquadId { get; set; }
        [Required]
        public Squad Squad { get; set; }

        public ICollection<SquadCheckIn> SquadCheckIns { get; set; }
    }
}