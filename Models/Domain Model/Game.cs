﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace HVZapi.Models.Domain_Model
{
    /// <summary>
    /// The Game class.
    /// Game entity
    /// </summary>
    public class Game
    {
        public int Id { get; set; }

        [Required]
        [StringLength(100)]
        public string Name { get; set; }

        // Game states
        [Required]
        public bool GameStarted { get; set; }
        [Required]
        public bool RegistrationOpen { get; set; }
        [Required]
        public bool GameComplete { get; set; }

        // Image
        [StringLength(100)]
        public string ImageUrl { get; set; }

        // Time
        [Required]
        [DataType(DataType.DateTime)]
        public DateTime StartTime { get; set; }
        [Required]
        [DataType(DataType.DateTime)]
        public DateTime EndTime { get; set; }

        // Coordinates
        [Required]
        [Column(TypeName = "decimal(17,15)")]

        public decimal NW_lat { get; set; }
        [Required]
        [Column(TypeName = "decimal(18,15)")]
        public decimal NW_lng { get; set; }
        [Required]
        [Column(TypeName = "decimal(17,15)")]
        public decimal SE_lat { get; set; }
        [Required]
        [Column(TypeName = "decimal(18,15)")]
        public decimal SE_lng { get; set; }
        public ICollection<Kill> Kills { get; set; }
        public ICollection<Chat> Chats { get; set; }
        public ICollection<Mission> Missions { get; set; }
        public ICollection<Player> Players { get; set; }
        public ICollection<Squad> Squads { get; set; }
        public ICollection<SquadCheckIn> SquadCheckIns { get; set; }
        public ICollection<SquadMember> SquadMembers { get; set; }
    }
}
