﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace HVZapi.Models.Domain_Model
{
    /// <summary>
    /// The User class.
    ///  User entity
    /// </summary>
    public class User
    {
        public int Id { get; set; }
        [Required]
        [StringLength(100)]
        [RegularExpression(@"^[a-zA-Z''-'\s]$",
         ErrorMessage = "Characters are not allowed.")]
        [DataType(DataType.Text)]
        [Display(Order = 1, Name = "First Name")]
        public string FirstName { get; set; }
        [Required]
        [StringLength(100)]
        [RegularExpression(@"^[a-zA-Z''-'\s]$",
         ErrorMessage = "Characters are not allowed.")]
        [DataType(DataType.Text)]
        [Display(Order = 2, Name = "Last Name")]
        public string LastName { get; set; }
        [Required]
        [EmailAddress]
        [Display(Name = "Email")]
        public string Email { get; set; }

        [StringLength(50)]
        public string Username { get; set; }

        public byte[] PasswordHash { get; set; }
        public byte[] PasswordSalt { get; set; }
        public int Age { get; set; }
        [Required]
        public string Phone { get; set; }
        [Required]
        [StringLength(30)]
        public string Gender { get; set; }

        [StringLength(50)]
        // Validation IMPORTANT
        public string Password { get; set; }
        public string  Role { get; set; }

        // Foreign keys
        public ICollection<Player> Players { get; set; }

    }
}
