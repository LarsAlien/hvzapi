﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HVZapi.Models.Domain_Model
{
    /// <summary>
    /// The Role class.
    ///  Role entity
    /// </summary>
    public static class Role
    {
        public const string Admin = "Admin";
        public const string User = "User";
    }
}
