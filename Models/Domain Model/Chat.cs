﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace HVZapi.Models.Domain_Model
{
    /// <summary>
    /// The Chat class.
    /// Chat entity
    /// </summary>
    public class Chat
    {
        public int Id { get; set; }
        [Required]
        [StringLength(150)]
        public string Message { get; set; }
        [Required]
        public bool IsHumanGlobal { get; set; }
        [Required]
        public bool IsZombieGlobal { get; set; }
        [Required]
        [DataType(DataType.DateTime)]
        public DateTime ChatTime { get; set; }

        [ForeignKey("SquadId")]
        public int? SquadId { get; set; }
        public Squad Squad { get; set; }


        [ForeignKey("GameId")]
        public int? GameId { get; set; }
        public Game Game { get; set; }

        [Required]
        [ForeignKey("PlayerId")]
        public int PlayerId { get; set; }
        public Player Player { get; set; }
    }
}
