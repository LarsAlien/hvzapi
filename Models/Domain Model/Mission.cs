﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace HVZapi.Models.Domain_Model
{
    /// <summary>
    /// The Mission class.
    /// Mission entity
    /// </summary>
    public class Mission
    {
        public int Id { get; set; }
        [Required]
        [StringLength(100)]
        public string Name { get; set; }
        [Required]
        public bool IsHumanVisible { get; set; }
        [Required]
        public bool IsZombieVisible { get; set; }
        [Required]
        [MaxLength]
        public string Description { get; set; }
        [Required]
        [DataType(DataType.DateTime)]
        public DateTime StartTime { get; set; }
        [Required]
        [DataType(DataType.DateTime)]
        public DateTime EndTime { get; set; }
        [Required]
        [Column(TypeName = "decimal(17,15)")]
        public decimal lat { get; set; }
        [Required]
        [Column(TypeName = "decimal(18,15)")]
        public decimal lng { get; set; }

        [Required]
        [ForeignKey("GameId")]
        public int GameId { get; set; }
        public Game Game { get; set; }
        [Required]
        public bool IsComplete { get; set; }

    }
}
