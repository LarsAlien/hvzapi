using HVZapi.Models.Domain_Model;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using HVZapi.Helpers;
using HVZapi.Helpers.SeedData;
using HVZapi.Services;

namespace HVZapi.Models
{
    /// <summary>
    ///  GameDbContext class.
    /// Include context to all tabels in database
    /// </summary>
    
    public class GameDbContext : DbContext
    {
        // Tables
        public DbSet<Game> Games { get; set; }
        public DbSet<Player> Players { get; set; }
        public DbSet<Squad> Squads { get; set; }
        public DbSet<Kill> Kills { get; set; }
        public DbSet<Mission> Missions { get; set; }
        public DbSet<User> Users { get; set; }
        public DbSet<Chat> Chats { get; set; }
        public DbSet<SquadCheckIn> SquadCheckIns { get; set; }
        public DbSet<SquadMember> SquadMembers { get; set; }

        public GameDbContext(DbContextOptions options) : base(options)
        {
        }

        // Seeding
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {

            modelBuilder.Entity<Kill>(k =>
            {
                k.HasOne(m => m.Killer)
                   .WithMany(t => t.KillerKills)
                   .HasForeignKey(m => m.KillerID).IsRequired(false);

                k.HasOne(m => m.Victim)
                        .WithMany(t => t.VictimKills)
                        .HasForeignKey(m => m.VictimID).IsRequired(false);
            });

            modelBuilder.Entity<Player>(s =>
            {
                s.HasOne(m => m.SquadMember)
                    .WithOne(p => p.Player)
                    .HasForeignKey<SquadMember>(p => p.PlayerId)
                    .OnDelete(DeleteBehavior.Restrict);
            });
            modelBuilder.Entity<Player>().ToTable("SquadMembers");
            modelBuilder.Entity<SquadMember>().ToTable("Players");
            modelBuilder.Entity<SquadMember>(s =>
            {

                s.HasOne(m => m.Squad)
                        .WithMany(t => t.SquadMembers)
                        .HasForeignKey(m => m.SquadId)
                        .OnDelete(DeleteBehavior.Restrict);
            });

            modelBuilder.Entity<SquadCheckIn>(s =>
            {
                s.HasOne(m => m.SquadMember)
                .WithMany(t => t.SquadCheckIns)
                .HasForeignKey(m => m.SquadMemberId)
                .OnDelete(DeleteBehavior.Restrict);

                s.HasOne(m => m.Squad)
                .WithMany(t => t.SquadCheckIns)
                .HasForeignKey(m => m.SquadId)
                .OnDelete(DeleteBehavior.Restrict);
            });
            DbSeeder.SeedAll();

            modelBuilder.Entity<Game>().HasData(DbSeeder.SeedGameData());
            List<User> users = DbSeeder.SeedUserData();
            foreach (var user in users)
            {
                byte[] passwordHash, passwordSalt;
                UserService.CreatePasswordHash(user.Password, out passwordHash, out passwordSalt);

                user.PasswordHash = passwordHash;
                user.PasswordSalt = passwordSalt;
                user.Password = null;
            }
            modelBuilder.Entity<User>().HasData(users);

            modelBuilder.Entity<SquadMember>().HasData(DbSeeder.SeedSquadMemberData());

            modelBuilder.Entity<Player>().HasData(DbSeeder.SeedPlayerData());

            modelBuilder.Entity<Squad>().HasData(DbSeeder.SeedSquadData());

            modelBuilder.Entity<Kill>().HasData(DbSeeder.SeedKillData());

            modelBuilder.Entity<Mission>().HasData(DbSeeder.SeedMissionData());

            modelBuilder.Entity<SquadCheckIn>().HasData(DbSeeder.SeedSquadCheckInData());


            modelBuilder.Entity<Chat>().HasData(DbSeeder.SeedChatData());

            base.OnModelCreating(modelBuilder);

        }

        private void CreatePasswordHash(object password, out byte[] passwordHash, out byte[] passwordSalt)
        {
            throw new NotImplementedException();
        }
    }
}
