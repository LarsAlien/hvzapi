# Humans vs Zombies Game Management API

HvZapi exposes endpoints for manipulating Games and users involved in HvZ-games. Instructions for setting this up is in this README.
## Built with
* AspNetCore
* EntityFrameworkCore
* Endpoints are documented with Swagger(SwashBuckle)
## Installation
1. Download solution and open in preferred IDE
2. Make sure following nuget packages is installed(Tools->NugetPackageManager->Installed):
    * AutoMapper
    * AutoMapper.Extensions.Microsoft.DependencyInjection
    * Microsoft.EntityFrameworkCore
    * Microsoft.EntityFrameworkCore.Design
    * Microsoft.EntityFrameworkCore.SqlServer
    * Microsoft.EntityFrameworkCore.Tools
    * SwashBuckle.AspNetCore
3. Make sure to edit "DefaultConnection" in appsettings.json to match your local sql server
It should look something like this when you are done. 
(create file `appsettings.json` in root directory if not exist)
```
{
  "Logging": {
    "LogLevel": {
      "Default": "Information",
      "Microsoft": "Warning",
      "Microsoft.Hosting.Lifetime": "Information"
    }
  },
  "AllowedHosts": "*",
  "ConnectionStrings": {
    "DefaultConnection": "Data Source=DESKTOP-434KLJIKj343\\SQLEXPRESS; Initial Catalog=HvZDb; Integrated Security=True"
  },
  "AppSettings": {
    "Secret": "AddYourOwnCustomSecret"
  }
}
```
4. The "AppSettings" block is where you create your own secret. (It has to do with client Authentication, bear with me)
5. Open Tools->Nuget package manager->Package manager console
    * RUN: `add-migration InitialDb` (Make the first migrations)
    * RUN: `update-database` (Creates the database on your sqlexpress server)
6. Run "ISS Express" server and Swagger UI with all endpoints opens in your browser.

## Authentication
This API is setup with an internal Authentication provider. 

