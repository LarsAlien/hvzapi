﻿using AutoMapper;
using HVZapi.Models.Data_Transfer_Objects;
using HVZapi.Models.Domain_Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HVZapi.Mappers
{
    public class MissionProfile:Profile
    {
        public MissionProfile()
        {
            CreateMap<Mission, GetMissionDTO>()
                .ForMember(m => m.Lat, s => s.MapFrom( sd => sd.lat))
                .ForMember(m => m.Lng, s => s.MapFrom( sd => sd.lng))
                .ReverseMap();
            CreateMap<PostMissionDTO, Mission>().ReverseMap();
            CreateMap<Mission, PutMissionDTO>().ReverseMap();
        }
    }
}
