﻿using AutoMapper;
using HVZapi.Models.Data_Transfer_Objects;
using HVZapi.Models.Domain_Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HVZapi.Mappers
{
    public class GameProfile : Profile
    {
        public GameProfile()
        {
            CreateMap<Game, GetGameDTO>()
                .ForMember(g => g.CountPlayers, d => d.MapFrom(sd => sd.Players.Count));
            CreateMap<GetGameDTO,Game>();
            CreateMap<PostGameDTO,Game>().ReverseMap();
            CreateMap<PutGameDTO,Game>().ReverseMap();

            //Player mapping
            CreateMap<Player, GetPlayerDTO>().ForMember(s => s.Name, d => d.MapFrom(sd => sd.User.FirstName + " " + sd.User.LastName));
            CreateMap<Player, GetPlayerWithSquadDTO>()
                .ForMember(s => s.Name, d => d.MapFrom(sd => sd.User.FirstName + " " + sd.User.LastName))
                .ForMember(s => s.SquadName, d => d.MapFrom(sd => sd.SquadMember.Squad.Name))
                .ForMember(s => s.Squad, d => d.MapFrom(sd => sd.SquadMember.Squad));
            CreateMap<Player, GetPlayerWithAllStatsDTO>()
                .ForMember(p => p.Squad, d => d.MapFrom(sd => sd.SquadMember.Squad));

            CreateMap<Player, PostPlayerDTO>().ReverseMap();
            CreateMap<GetPlayerDTO, Player>();

        }
    }
}
