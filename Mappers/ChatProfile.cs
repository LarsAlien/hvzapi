﻿using AutoMapper;
using HVZapi.Models.Data_Transfer_Objects;
using HVZapi.Models.Domain_Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HVZapi.Mappers
{
    public class ChatProfile : Profile
    {
        public ChatProfile()
        {
            // Chat mapping
            CreateMap<Chat, GetChatDTO>().ForMember(c => c.Username, d => d.MapFrom(sd =>
                                                        sd.Player.User.Username));
            CreateMap<GetChatDTO, Chat>();
            CreateMap<Chat, PostGlobalChatDTO>().ReverseMap();
            CreateMap<Chat, PostSquadChatDTO>().ReverseMap();
        }
    }
}
