﻿using AutoMapper;
using HVZapi.Models.Data_Transfer_Objects;
using HVZapi.Models.Domain_Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HVZapi.Mappers
{
    public class SquadProfile : Profile
    {
        public SquadProfile()
        {
            CreateMap<Squad, GetSquadAndMembersDTO>()
                .ForMember(s => s.SquadMembers,
                            d => d.MapFrom(sd => sd.SquadMembers));
            CreateMap<Squad, GetSquadDTO>().ReverseMap();

            CreateMap<Squad, PostSquadDTO>().ReverseMap();

            CreateMap<Squad, GetFullSquadInfo>()
                .ForMember(s => s.SquadMembers,
                            d => d.MapFrom(sd => sd.SquadMembers))
                .ForMember(s => s.SquadCheckins, d => d.MapFrom(sd => sd.SquadCheckIns));

        }
    }
}
