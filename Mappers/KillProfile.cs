﻿using AutoMapper;
using HVZapi.Models.Data_Transfer_Objects;
using HVZapi.Models.Domain_Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HVZapi.Mappers
{
    public class KillProfile : Profile
    {
        public KillProfile()
        {
            CreateMap<Kill, GetKillDTO>().ReverseMap();
            CreateMap<PostKillDTO, Kill>().ReverseMap();
        }
    }
}
