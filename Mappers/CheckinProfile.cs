﻿using AutoMapper;
using HVZapi.Models.Data_Transfer_Objects;
using HVZapi.Models.Domain_Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HVZapi.Mappers
{
    public class CheckinProfile : Profile
    {
        public CheckinProfile()
        {
            CreateMap<SquadCheckIn, GetCheckinDTO>().ReverseMap();
            CreateMap<SquadCheckIn, PostCheckinDTO>().ReverseMap();
        }
    }
}
