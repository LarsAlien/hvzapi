﻿using AutoMapper;
using HVZapi.Models.Data_Transfer_Objects;
using HVZapi.Models.Domain_Model;
using WebApi.Models.Users;

namespace HVZapi.Mappers
{
    public class UserProfile:Profile
    {
        public UserProfile()
        {
            CreateMap<User, UserinfoDTO>();
            CreateMap<RegisterDTO, User>();
            CreateMap<UpdateUserDTO, User>();
        }
    }
}
