﻿using AutoMapper;
using HVZapi.Models.Data_Transfer_Objects;
using HVZapi.Models.Domain_Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HVZapi.Mappers
{
    public class SquadMemberProfile : Profile
    {
        public SquadMemberProfile()
        {
            //CreateMap<SquadMember, GetSquadMemberDTO>().ReverseMap();
            CreateMap<SquadMember, PostSquadMemberDTO>().ReverseMap();

            CreateMap<SquadMember, GetSquadMemberDTO>()
                .ForMember(s => s.Name, d => d.MapFrom(sd => sd.Player.User.FirstName +" "+ sd.Player.User.LastName))
                .ForMember(s => s.IsHuman, d => d.MapFrom(sd => sd.Player.IsHuman));
        }
    }
}
