﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HVZapi.Helpers
{
    /// <summary>
    /// The AppSettings class.
    /// This class used to get access to config in appsettings.json
    /// </summary>

    public class AppSettings
    {
        public string Secret { get; set; }
    }
}
