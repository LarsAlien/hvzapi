﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HVZapi.Helpers
{
    /// <summary>
    /// The NotAuthorizedException class.
    /// Thrown when the user is not Authorized
    /// </summary>
    public class NotAuthorizedException : Exception
    {
        public NotAuthorizedException() : base()
        {
        }

        public NotAuthorizedException(string message) : base(message)
        {
        }

        public NotAuthorizedException(string message, Exception innerException) : base(message, innerException)
        {
        }
    }
}
