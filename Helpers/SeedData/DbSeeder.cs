﻿using HVZapi.Models.Domain_Model;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace HVZapi.Helpers.SeedData
{

    /// <summary>
    /// The DbSeeder class.
    /// Contains all methods for seeding all tabels.
    /// </summary>
    public static class DbSeeder
    {
        static List<Game> allGames = new List<Game>();
        static List<User> allUsers = new List<User>();

        static List<Player> allPlayers = new List<Player>();

        static List<Squad> allSquads = new List<Squad>();
        static List<SquadMember> allSquadMembers = new List<SquadMember>();
        static List<SquadCheckIn> allSquadCheckIns = new List<SquadCheckIn>();
        static List<Kill> allKills = new List<Kill>();
        static List<Mission> allMissions = new List<Mission>();
        static List<Chat> allChats = new List<Chat>();

        // Seed All tabels in the database
        /// <summary>
        /// Seed All tabels in the database
        /// </summary>
        public static void SeedAll()
        {
            allGames = SeedGameData();
            allUsers = SeedUserData();
            SeedPlayerDataHelper();
            SeedSquadDataHelper();
            SeedChatDataHelper();
            SeedKillDataHelper();
            SeedSquadCheckInDataHelper();
            SeedMissionDataHelper();
        }

        // Convert data from jsonform to list of games
        /// <summary>
        /// Convert data from jsonform to list of games
        /// </summary>
        public static List<Game> SeedGameData()
        {
            string filePath = @"./Helpers/SeedData/Game.json";
            return JsonConvert.DeserializeObject<List<Game>>(ReadJson(filePath));
        }

    
        public static List<SquadMember> SeedSquadMemberData()
        {
            return allSquadMembers;
        }
        public static List<Player> SeedPlayerData()
        {
            return allPlayers;
        }
        public static List<Squad> SeedSquadData()
        {
            return allSquads;
        }
        public static List<Kill> SeedKillData()
        {
            return allKills;
        }
        public static List<Mission> SeedMissionData()
        {
            return allMissions;
        }
        // Convert data from jsonform to list of user
        /// <summary>
        /// Convert data from jsonform to list of user
        /// </summary>
        public static List<User> SeedUserData()
        {
            string filePath = @"./Helpers/SeedData/User.json";
            return JsonConvert.DeserializeObject<List<User>>(ReadJson(filePath));
        }
        public static List<SquadCheckIn> SeedSquadCheckInData()
        {
            return allSquadCheckIns;
        }
        public static List<Chat> SeedChatData()
        {
            return allChats;
        }
        // Read json data using StreamReader
        /// <summary>
        /// Read json data using StreamReader
        /// </summary>
        static string ReadJson(string filePath)
        {
            using (StreamReader r = new StreamReader(filePath))
            {
                return r.ReadToEnd();
            }
        }
        // Seed Player Table with random players
        /// <summary>
        /// Seed Player Table with random players
        /// </summary>
        public static void SeedPlayerDataHelper()
        {
            int currentPlayerIndex = 1;
            for (int i = 0; i < allGames.Count; i++)
            {
                int currentUserIndex = 2;
                for (int j = 1; j < 4; j++)
                {
                    Player player = new Player();

                    player.Id = currentPlayerIndex;
                    currentPlayerIndex++;
                    player.IsHuman = true;
                    player.IsPatientZero = false;
                    player.UserId = currentUserIndex;
                    currentUserIndex++;
                    player.BiteCode = $"Bite{i}{currentPlayerIndex}{player.UserId}";
                    player.GameId = i +1;
                    allPlayers.Add(player);

                    Player player1 = new Player();

                    player1.Id = currentPlayerIndex;
                    currentPlayerIndex++;
                    player1.IsHuman = false;
                    player1.IsPatientZero = true;
                    player1.UserId = currentUserIndex;
                    currentUserIndex++;
                    player1.BiteCode = $"Bite{i}{currentPlayerIndex}{player1.UserId}";
                    player1.GameId = i + 1;
                    allPlayers.Add(player1);

                    Player player2 = new Player();

                    player2.Id = currentPlayerIndex;
                    currentPlayerIndex++;
                    player2.IsHuman = false;
                    player2.IsPatientZero = false;
                    player2.UserId = currentUserIndex;
                    currentUserIndex++;
                    player2.BiteCode = $"Bite{i}{currentPlayerIndex}{player2.UserId}";
                    player2.GameId = i + 1;
                    allPlayers.Add(player2);
                }

            }
        }

        // Seed Squad Table with random Squads
        /// <summary>
        /// Seed Squad Table with random Squads
        /// </summary>
        public static void SeedSquadDataHelper()
        {
            int currentSquadIndex = 1;
            int currentSquadMemberId = 1;
            for (int i = 0; i < allGames.Count; i++)
            {
                List<Player> players = allPlayers.FindAll(p => p.GameId == i + 1);
                int currentPlayer = 0;

                Squad squad = new Squad();
                squad.Id = currentSquadIndex;
                currentSquadIndex++;
                squad.Name = "Goon squad";
                squad.GameId = i + 1;
                allSquads.Add(squad);

                SquadMember sm = new SquadMember();
                sm.Id = currentSquadMemberId;
                sm.Rank = "Captain";
                currentSquadMemberId++;
                sm.GameId = i + 1;
                sm.SquadId = squad.Id;
                sm.PlayerId = players[currentPlayer].Id;
                currentPlayer++;
                allSquadMembers.Add(sm);

                SquadMember sm1 = new SquadMember();
                sm1.Id = currentSquadMemberId;
                sm1.Rank = "Rook";
                currentSquadMemberId++;
                sm1.GameId = i + 1;
                sm1.SquadId = squad.Id;
                sm1.PlayerId = players[currentPlayer].Id;
                currentPlayer++;
                allSquadMembers.Add(sm1);

                Squad squad1 = new Squad();
                squad1.Id = currentSquadIndex;
                currentSquadIndex++;
                squad1.Name = "B-Gjengen";
                squad1.GameId = i + 1;
                allSquads.Add(squad1);

                SquadMember sm2 = new SquadMember();
                sm2.Id = currentSquadMemberId;
                sm2.Rank = "Captain";
                currentSquadMemberId++;
                sm2.GameId = i + 1;
                sm2.SquadId = squad1.Id;
                sm2.PlayerId = players[currentPlayer].Id;
                currentPlayer++;
                allSquadMembers.Add(sm2);

                SquadMember sm3 = new SquadMember();
                sm3.Id = currentSquadMemberId;
                sm3.Rank = "Rook";
                currentSquadMemberId++;
                sm3.GameId = i + 1;
                sm3.SquadId = squad1.Id;
                sm3.PlayerId = players[currentPlayer].Id;
                currentPlayer++;
                allSquadMembers.Add(sm3);
            }
        }


        // Seed Chat  Table with random Chats
        /// <summary>
        /// Seed Chat Table with random Chats
        /// </summary>
        public static void SeedChatDataHelper()
        {
            int currentChatIndex = 1;
            for (int i = 0; i < allGames.Count; i++)
            {
                Random random = new Random();
                List<Player> players = allPlayers.FindAll(p => p.GameId == i + 1);

                List<Squad> squads = allSquads.FindAll(p => p.GameId == i + 1);
                int currentSquadIndex = 0;
                Chat chatGlobal = new Chat();
                chatGlobal.Id = currentChatIndex;
                currentChatIndex++;
                chatGlobal.ChatTime = DateTime.Now;
                chatGlobal.IsHumanGlobal = true;
                chatGlobal.IsZombieGlobal = false;
                chatGlobal.Message = "Global Human";
                chatGlobal.GameId = i + 1;
                chatGlobal.PlayerId = players[random.Next(players.Count)].Id;
                allChats.Add(chatGlobal);

                Chat chatGlobal1 = new Chat();
                chatGlobal1.Id = currentChatIndex;
                currentChatIndex++;
                chatGlobal1.ChatTime = DateTime.Now;
                chatGlobal1.IsHumanGlobal = false;
                chatGlobal1.IsZombieGlobal = true;
                chatGlobal1.Message = "AGlobal zombie";
                chatGlobal1.GameId = i + 1;
                chatGlobal1.PlayerId = players[random.Next(players.Count)].Id;
                allChats.Add(chatGlobal1);

                Chat chatGlobal2 = new Chat();
                chatGlobal2.Id = currentChatIndex;
                currentChatIndex++;
                chatGlobal2.ChatTime = DateTime.Now;
                chatGlobal2.IsHumanGlobal = false;
                chatGlobal2.IsZombieGlobal = false;
                chatGlobal2.Message = "Global";
                chatGlobal2.GameId = i + 1;
                chatGlobal2.PlayerId = players[random.Next(players.Count)].Id;
                allChats.Add(chatGlobal2);

                for (int j = 0; j < squads.Count; j++)
                {
                    Chat chatSquad = new Chat();
                    chatSquad.Id = currentChatIndex;
                    currentChatIndex++;
                    chatSquad.ChatTime = DateTime.Now;
                    chatSquad.Message = "Squad message";
                    chatSquad.SquadId = squads[currentSquadIndex].Id;
                    currentSquadIndex++;
                    chatSquad.PlayerId = players[random.Next(players.Count)].Id;
                    allChats.Add(chatSquad);

                }
            }
        }


        // Seed Kill Table with random Kills
        /// <summary>
        /// Seed Kill Table with random Kills
        /// </summary>
        public static void SeedKillDataHelper()
        {
            for (int i = 0; i < allGames.Count; i++)
            {
                List<Player> players = allPlayers.FindAll(p => p.GameId == i + 1);

                Kill kill = new Kill()
                {
                    Id = i + 1,
                    GameId = i + 1,
                    IsVisible = true,
                    Story = "Story",
                    Lat = 59.9213924958088m,
                    Lng = 10.80503367970039m,
                    KillerID = players[1].Id,
                    VictimID = players[2].Id,
                    TOD = DateTime.Now
                };
                allKills.Add(kill);
            }
        }


        // Seed SquadCheckIn Table with random SquadCheckIns
        /// <summary>
        /// Seed SquadCheckIn Table with random SquadCheckIns
        /// </summary>
        public static void SeedSquadCheckInDataHelper()
        {
            int currentIndex = 1;
            for (int i = 0; i < allGames.Count; i++)
            {
                List<Squad> squads = allSquads.FindAll(p => p.GameId == i + 1);

                for (int j = 0; j < squads.Count; j++)
                {
                    Random r = new Random();
                    List<SquadMember> sms = allSquadMembers.FindAll(p => p.SquadId == squads[j].Id);
                    SquadCheckIn sc = new SquadCheckIn()
                    {
                        Id = currentIndex,
                        StartTime = DateTime.Now,
                        EndTime = DateTime.Now.AddDays(1),
                        GameId = i + 1,
                        SquadMemberId = sms[r.Next(sms.Count - 1)].Id,
                        SquadId = squads[j].Id,
                        lat = 59.9213924958088m,
                        lng = 10.80503367970039m
                    };
                    currentIndex++;
                    allSquadCheckIns.Add(sc);
                }

            }
        }

        // Seed Mission Table with random Missions
        /// <summary>
        /// Seed Mission Table with random Missions
        /// </summary>
        public static void SeedMissionDataHelper()
        {
            int currentIndex = 1;
            for (int i = 0; i < allGames.Count; i++)
            {
                Mission m = new Mission()
                {
                    Id = currentIndex,
                    StartTime = DateTime.Now,
                    EndTime = DateTime.Now.AddDays(1),
                    GameId = i + 1,
                    lat = 59.9213924958088m,
                    lng = 10.80503367970039m,
                    Description = "MissionDesc",
                    IsHumanVisible = false,
                    IsZombieVisible = true,
                    IsComplete = false,
                    Name = $"Kill princess {i+1}"
                };
                currentIndex++;
                allMissions.Add(m);

                Mission m1 = new Mission()
                {
                    Id = currentIndex,
                    StartTime = DateTime.Now,
                    EndTime = DateTime.Now.AddDays(1),
                    GameId = i + 1,
                    lat = 59.9213924958088m,
                    lng = 10.80503367970039m,
                    Description = "MissionDesc",
                    IsHumanVisible = true,
                    IsZombieVisible = false,
                    IsComplete = false,
                    Name = $"Rescue princess {i + 1}"
                };
                currentIndex++;
                allMissions.Add(m1);

            }
        }
    }
}
