﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HVZapi.Helpers
{
    /// <summary>
    /// The BiteCodeGeneratorclass.
    /// Generete the BiteCode 
    /// </summary>
    public class BiteCodeGenerator
    {
        public static string Generator()
        {

          
            Random random = new Random();
            StringBuilder bitecode = new StringBuilder();

            const string chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";
            const string nums = "123456789";

            char[] letters = Enumerable.Repeat(chars, 4)
             .Select(s => s[random.Next(s.Length)]).ToArray();
            char[] numbers = Enumerable.Repeat(nums, 3)
             .Select(s => s[random.Next(s.Length)]).ToArray();
            bitecode.Append(letters).Append(numbers);
            return bitecode.ToString();

        }
    }
}
