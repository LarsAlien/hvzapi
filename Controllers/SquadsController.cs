﻿using HVZapi.Helpers;
using HVZapi.Models.Data_Transfer_Objects;
using HVZapi.Models.Domain_Model;
using HVZapi.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HVZapi.Controllers
{
    [Authorize]
    [Produces("application/json")]
    [Route("api/squads")]
    [ApiController]
    public class SquadsController : Controller
    {
        private ISquadService _squadService;

        public SquadsController(ISquadService squadService)
        {
            _squadService = squadService;
        }
        /// <summary>
        /// Get all Squads
        /// </summary>
        /// <returns>A list of Squad objects</returns>
        /// <response code="200">Succesfull Ok response </response>
        //GET All squads
        [ProducesResponseType(StatusCodes.Status200OK)]
        [HttpGet]
        public Task<IEnumerable<GetSquadDTO>> Get()
        {
            return _squadService.GetSquads();
        }

        /// <summary>
        /// Get squad by squadId
        /// </summary>
        /// <param name="squadId"></param>
        /// <returns>A single Squad Object</returns>
        /// <response code="200">Succesfull Ok response with entitiy </response>
        /// <response code="404">Requested resource could not be found </response>
        /// <response code="403">Access denied</response>
        //GET: api/squads/{squadId}
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [HttpGet("{squadId}")]
        public async Task<ActionResult<GetSquadAndMembersDTO>> Get(int squadId)
        {



            try
            {
                return await _squadService.GetSquad(squadId, User);
            }
            catch (NotAuthorizedException)
            {
                return Forbid();
            }
            catch (AppException ex)
            {
                return NotFound(new { message = ex.Message });
            }
        }

        /// <summary>
        /// Update existing Squad Entity
        /// </summary>
        /// <param name="squadDTO"></param>
        /// <param name="squadId"></param>
        /// <response code="204">Succesfull reponse, no content </response>
        /// <response code="404">Requested resource could not be found </response>
        /// <response code="400">Server cannot process the request</response>
        // PUT: api/[controller]/{id}
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [HttpPut("{squadId}")]
        public IActionResult UpdateSquad(int squadId, GetSquadDTO squadDTO)
        {
            if (squadId != squadDTO.Id)
            {
                return BadRequest();
            }
            try
            {
                _squadService.UpdateSquad(squadDTO);
                return NoContent();
            }

            catch (AppException ex)
            {
                return NotFound(new { message = ex.Message });
            }


        }

        /// <summary>
        /// Add new Squad Entity
        /// </summary>
        /// <param name="squadDTO"></param>
        /// <returns>Newly created Squad Objective</returns>
        /// <response code="400">Server cannot process the request</response>
        /// <response code="200">Succesfull Ok response with entitiy </response>
        // POST: api/squads
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [HttpPost]
        public async Task<ActionResult<GetSquadDTO>> Post(PostSquadDTO squadDTO)
        {
            try
            {
                GetSquadDTO squad = await _squadService.CreateSquad(squadDTO, User);
                return CreatedAtAction("Get", new { id = squad.Id }, squad);
            }
            catch (DbUpdateConcurrencyException ex)
            {
                return BadRequest($"Could not add a new squad: {ex.Message}");
            }
            catch (AppException ex)
            {
                return BadRequest(new { message = ex.Message });
            }
        }

        /// <summary>
        /// Add new SquadMember entity (Method for joing squad)
        /// </summary>
        /// <param name="squadMemberDTO"></param>
        /// <returns>Ok response including Squad Member Object</returns>
        /// <response code="400">Server cannot process the request</response>
        /// <response code="200">Succesfull Ok response with entitiy </response>
        // POST: api/squads/{squadId}/join
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [HttpPost("{squadId}/join")]
        public async Task<ActionResult<GetSquadMemberDTO>> JoinSquad(PostSquadMemberDTO squadMemberDTO)
        {
            try
            {
                GetSquadMemberDTO squadMember = await _squadService.JoinSquad(squadMemberDTO);
                return Ok(squadMember);
            }

            catch (DbUpdateConcurrencyException ex)
            {
                return BadRequest($"Could not create new squadmember: {ex.Message}");
            }
        }

        /// <summary>
        /// Leave squad, deletes squadmember entity
        /// </summary>
        /// <param name="squadMemberDTO"></param>
        /// <returns>Ok response including Squad Member Object</returns>
        /// <response code="400">Server cannot process the request</response>
        /// <response code="200">Succesfull Ok response with entitiy </response>
        // POST: api/squads/{squadId}/join
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [HttpDelete("{squadId}/leave")]
        public ActionResult LeaveSquad(int squadId)
        {
            try
            {
                _squadService.LeaveSquad(squadId, User);
                return Ok();
            }
            catch (DbUpdateConcurrencyException ex)
            {
                return BadRequest($"Could not delete squadmember: {ex.Message}");
            }
            catch (Exception ex)
            {
                return BadRequest($"Could not delete squadmember: {ex.Message}");
            }
        }

        /// <summary>
        /// Delete Squad Entity by squadId
        /// </summary>
        /// <param name="squadId"></param>
        /// <returns> Deleted Squad Object wrapped in ActionResult</returns>
        /// <response code="200">Succesfull Ok response with entitiy </response>
        /// <response code="404">Requested resource could not be found </response>
        /// <response code="400">Server cannot process the request</response>
        // DELETE: api/squads/{id}
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [HttpDelete("{squadId}")]
        [Authorize(Roles = Role.Admin)]
        public ActionResult Delete(int squadId)
        {
            try
            {
                _squadService.DeleteSquad(squadId);
                return Ok();
            }
            catch (DbUpdateConcurrencyException ex)
            {
                return BadRequest($"Could not delete squad: {ex.Message}");
            }
            catch (AppException ex)
            {
                return BadRequest($"Could not delete squad: {ex.Message}");
            }
        }
        /// <summary>
        /// Get chats with specific squadId
        /// </summary>
        /// <param name="squadId"></param>
        /// <returns>Chat Object belonging to squad</returns>
        /// <response code="200">Succesfull Ok response with entitiy </response>
        /// <response code="400">Server cannot process the request</response>
        /// <response code="403">Access denied</response>
        //GET: api/squads/{squadId}/chat
        [HttpGet("{squadId}/chat")]
        public async Task<ActionResult<List<GetChatDTO>>> GetSquadChat(int squadId)
        {
            try
            {
                return await _squadService.GetChat(squadId, User);
            }
            catch (NotAuthorizedException)
            {
                return Forbid();
            }
            catch (AppException ex)
            {
                return NotFound(new { message = ex.Message });
            }
        }
        /// <summary>
        /// Add new Chat Entity
        /// </summary>
        /// <param name="chatDTO"></param>
        /// <returns>Newly created Chat Object</returns>
        /// <response code="200">Succesfull Ok response with entitiy </response>
        /// <response code="400">Server cannot process the request</response>
        /// <response code="403">Access denied</response>
        // POST: api/squads/{squadId}/chat
        [HttpPost("{squadId}/chat")]
        public async Task<ActionResult<GetChatDTO>> PostSquadChat(PostSquadChatDTO chatDTO)
        {
            try
            {
                return await _squadService.PostChat(chatDTO, User);
            }
            catch (NotAuthorizedException)
            {
                return Forbid();
            }
            catch (DbUpdateConcurrencyException ex)
            {
                return BadRequest($"Could not add a new chat: {ex.Message}");
            }

        }

        /// <summary>
        /// Get SquadCheckins with specific squadId
        /// </summary>
        /// <param name="squadId"></param>
        /// <returns>Squad checkins List Object</returns>
        /// <response code="200">Succesfull Ok response with entitiy </response>
        /// <response code="400">Server cannot process the request</response>
        /// <response code="403">Access denied</response>
        //GET: api/squads/{squadId}/check-in
        [HttpGet("{squadId}/check-in")]
        public async Task<ActionResult<List<GetCheckinDTO>>> GetSquadCheckin(int squadId)
        {
            try
            {
                return await _squadService.GetCheckin(squadId, User);
            }
            catch (NotAuthorizedException)
            {
                return Forbid();
            }
            catch (AppException ex)
            {
                return NotFound(new { message = ex.Message });
            }
        }


        /// <summary>
        /// Add new SquadCheckin Entity
        /// </summary>
        /// <param name="checkinDTO"></param>
        /// <reutrns>Newly created checkin Object</reutrns>
        /// <response code="200">Succesfull Ok response with entitiy </response>
        /// <response code="400">Server cannot process the request</response>
        /// <response code="403">Access denied</response>
        // POST: api/squads/{squadId}/check-in
        [HttpPost("{squadId}/check-in")]
        public async Task<ActionResult<GetCheckinDTO>> PostSquadCheckin(PostCheckinDTO checkinDTO)
        {
            GetCheckinDTO checkin;
            try
            {
                checkin = await _squadService.PostCheckin(checkinDTO, User);
            }
            catch (NotAuthorizedException)
            {
                return Forbid();
            }
            catch (DbUpdateConcurrencyException ex)
            {
                return BadRequest($"Could not add a new checkin: {ex.Message}");
            }
            return Ok(checkin);
        }
    }
}
