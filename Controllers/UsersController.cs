﻿using System;
using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using AutoMapper;
using System.IdentityModel.Tokens.Jwt;
using Microsoft.Extensions.Options;
using System.Text;
using Microsoft.IdentityModel.Tokens;
using System.Security.Claims;
using Microsoft.AspNetCore.Authorization;
using WebApi.Models.Users;
using HVZapi.Helpers;
using HVZapi.Models.Data_Transfer_Objects;
using HVZapi.Models.Domain_Model;
using HVZapi.Services;
using Microsoft.AspNetCore.Http;
using System.Threading.Tasks;

namespace HVZapi.Controllers
{

    [Authorize]
    [ApiController]
    [Route("api/[controller]")]
    public class UsersController : ControllerBase
    {
        private IUserService _userService;
        private IMapper _mapper;
        private readonly AppSettings _appSettings;

        public UsersController(
            IUserService userService,
            IMapper mapper,
            IOptions<AppSettings> appSettings)
        {
            _userService = userService;
            _mapper = mapper;
            _appSettings = appSettings.Value;
        }

        /// <summary>
        /// Login and authenticate
        /// </summary>
        /// <param name="lognDTO">Login object to Authenticate</param>
        /// <returns>User Object and authentication token</returns>
        /// <response code="400">Server cannot process the request</response>
        /// <response code="200">Succesfull Ok response with entitiy </response>
        // POST: api/users/authenticate
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [AllowAnonymous]
        [HttpPost("authenticate")]
        public IActionResult AuthenticateUser([FromBody] LoginDTO lognDTO)
        {
            var user = _userService.AuthenticateUser(lognDTO.Username, lognDTO.Password);

            if (user == null)
                return BadRequest(new { message = "Username or password is incorrect" });

            var tokenHandler = new JwtSecurityTokenHandler();
            var key = Encoding.ASCII.GetBytes(_appSettings.Secret);
            var tokenDescriptor = new SecurityTokenDescriptor
            {
                Subject = new ClaimsIdentity(new Claim[]
                {
                    new Claim(ClaimTypes.Name, user.Id.ToString()),
                    new Claim(ClaimTypes.Role,user.Role),
                    new Claim(ClaimTypes.Actor, user.Username)

                }),
                Expires = DateTime.UtcNow.AddDays(7),
                SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(key), SecurityAlgorithms.HmacSha256Signature)
            };
            var token = tokenHandler.CreateToken(tokenDescriptor);
            var tokenString = tokenHandler.WriteToken(token);

            // return basic user info and authentication token
            return Ok(new
            {
                Id = user.Id,
                Username = user.Username,
                FirstName = user.FirstName,
                LastName = user.LastName,
                Roles = user.Role,

                Token = tokenString
            });
        }


        /// <summary>
        /// Creates new user
        /// </summary>
        /// <param name="registerDTO">Register object</param>
        /// <returns>Ok</returns>
        /// <response code="400">Server cannot process the request</response>
        /// <response code="200">Succesfull Ok response with entitiy </response>
        // POST: api/users/register
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [AllowAnonymous]
        [HttpPost("register")]
        public IActionResult RegisterUser([FromBody] RegisterDTO registerDTO)
        {
            // map model to entity
            var user = _mapper.Map<User>(registerDTO);

            try
            {
                // create user
                _userService.CreateUser(user, registerDTO.Password);
                return Ok();
            }
            catch (AppException ex)
            {
                // return error message if there was an exception
                return BadRequest(new { message = ex.Message });
            }
        }


        /// <summary>
        /// Get all Users, Admin Only
        /// </summary>
        /// <returns>List of userInfo Objects</returns>
        /// <response code="200">Succesfull Ok response </response>
        // GET: api/users
        [ProducesResponseType(StatusCodes.Status200OK)]
        [Authorize(Roles = Role.Admin)]
        [HttpGet]
        public IActionResult GetAllUser()
        {
            var users = _userService.GetAllUser();
            var usersList = _mapper.Map<IList<UserinfoDTO>>(users);
            return Ok(usersList);
        }


        /// <summary>
        /// Get user by UserId
        /// </summary>
        /// <param name="id">Id for specific User</param>
        /// <returns>A single User Object</returns>
        /// <response code="200">Succesfull Ok response </response>
        /// <response code="403">Request data is valid, but server refuse action </response>
        // GET: api/users/{id}
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status403Forbidden)]
        [HttpGet("{id}")]
        public IActionResult GetUserById(int id)
        {

            // only allow admins to access other user records
            var currentUserId = int.Parse(User.Identity.Name);
            if (id != currentUserId && !User.IsInRole(Role.Admin))
                return Forbid();
            var user = _userService.GetUserById(id);
            var userDTO = _mapper.Map<UserinfoDTO>(user);
            return Ok(userDTO);
        }


        /// <summary>
        /// Update User
        /// </summary>
        /// <param name="id">Id for specific User</param>
        /// <param name="userToUpdate">Update User object</param>
        /// <returns>Ok</returns>
        /// <response code="200">Succesfull Ok response </response>
        /// <response code="403">Request data is valid, but server refuse action </response>
        /// <response code="400">Server cannot process the request</response>
        // PUT: api/users/{userId}
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status403Forbidden)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [HttpPut("{id}")]
        public IActionResult UpdateUserById(int id, [FromBody] UpdateUserDTO userToUpdate)
        {
            //only admin and user which own the account can update.
            var currentUserId = int.Parse(User.Identity.Name);
            if (id != currentUserId && !User.IsInRole(Role.Admin))
                return Forbid();
            // map model to entity and set id
            var user = _mapper.Map<User>(userToUpdate);
            user.Id = id;

            try
            {
                // update user
                _userService.UpdateUser(user, userToUpdate.Password);
                return Ok();
            }
            catch (AppException ex)
            {
                // return error message if there was an exception
                return BadRequest(new { message = ex.Message });
            }
        }


        /// <summary>
        /// Delete user
        /// </summary>
        /// <param name="id">Id for specific User</param>
        /// <returns>Ok</returns>
        /// <response code="200">Succesfull Ok response </response>
        /// <response code="403">Request data is valid, but server refuse action </response>
        // DELETE: api/users/{userId}
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status403Forbidden)]
        [HttpDelete("{id}")]
        public IActionResult DeleteUserById(int id)
        {
            //only admin and user which own the account can delete.
            var currentUserId = int.Parse(User.Identity.Name);
            if (id != currentUserId && !User.IsInRole(Role.Admin))
                return Forbid();
            _userService.DeleteUserById(id);
            return Ok();
        }

        /// <summary>
        /// Get all Players with specified GameId
        /// </summary>
        /// <param name="userId">Id for specific User Entity</param>
        /// <returns>A list with all Game objects with specific userId</returns>
        // GET: api/users/{userId}/games
        [HttpGet("{userId}/games")]
        public async Task<ActionResult<IEnumerable<GetPlayerWithAllStatsDTO>>> GetUserPlayers(int userId)
        {
            try
            {
                return await _userService.GetUserPlayers(userId);
            }
            catch (AppException ex)
            {
                return NotFound(new { message = ex.Message });
            }

        }
    }
}

