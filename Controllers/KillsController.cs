﻿using HVZapi.Helpers;
using HVZapi.Models.Data_Transfer_Objects;
using HVZapi.Models.Domain_Model;
using HVZapi.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace HVZapi.Controllers
{
    [Authorize]
    [Produces("application/json")]
    [Route("api/kills")]
    [ApiController]
    public class KillsController : Controller
    {
        private readonly IKillService _KillService;
        public KillsController(IKillService KillService)
        {
            _KillService = KillService;
        }

        /// <summary>
        /// Get all Kills
        /// </summary>
        /// <returns>A list of Kill objects</returns>
        /// <response code="200">Succesfull Ok response </response>
        // GET: api/kills
        [ProducesResponseType(StatusCodes.Status200OK)]
        [HttpGet]
        public Task<IEnumerable<GetKillDTO>> Get()
        {
            return _KillService.GetAllKillsAsync();
        }

        /// <summary>
        /// Get Kill by KillId
        /// </summary>
        /// <param name="killId">Id for specific Kill Entity</param>
        /// <returns>A single Kill Object</returns>
        /// <response code="200">Succesfull Ok response with entitiy </response>
        /// <response code="404">Requested resource could not be found </response>
        // GET: api/kills/{killId}/
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [HttpGet("{killId}")]
        public async Task<ActionResult<GetKillDTO>> GetKillById(int killId)
        {
            try
            {
                GetKillDTO kill = await _KillService.GetKillByIdAsync(killId);
                return kill;
            }
            catch(AppException ex)
            {
                return NotFound(new { message = ex.Message });
            }

        }

        /// <summary>
        /// Update existing Kill Entity
        /// </summary>
        /// <param name="killId">Id for specific Kill Entity</param>
        /// <param name="kill">Kill Object with updated values</param>
        /// <response code="204">Succesfull reponse, no content </response>
        /// <response code="404">Requested resource could not be found </response>
        /// <response code="400">Server cannot process the request</response>
        // PUT: api/kills/{killId}/
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [HttpPut("{killId}")]
        public IActionResult UpdateKill(int killId, GetKillDTO kill)
        {
            if (killId != kill.Id)
            {
                return BadRequest();
            }
            try
            {
                _KillService.UpdateKill(User, kill); // ikke to arguments?
                return NoContent();
            }
            catch (NotAuthorizedException ex)
            {
                return Forbid();
            }
            catch (AppException ex)
            {
                return NotFound(new { message = ex.Message });
            }
        }

        /// <summary>
        /// Delete Kill Entity by KillId
        /// </summary>
        /// <param name="killId">Id for specific Kill Entity</param>
        /// <returns>Deleted Kill object wrapped in ActionResult</returns>
        /// <response code="200">Succesfull Ok response with entitiy </response>
        /// <response code="404">Requested resource could not be found </response>
        // DELETE: api/kills/{killId}/
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [HttpDelete("{killId}")]
        [Authorize(Roles = Role.Admin)]
        public async Task<ActionResult<GetKillDTO>> Delete(int killId)
        {
            try {
                var kill = await _KillService.Delete(killId);
                return Ok(kill);
            }
            catch (AppException ex)
            {
                return NotFound (new { message = ex.Message });
            }

            catch (DbUpdateConcurrencyException ex)
            {
                return BadRequest($"Could not delete kill: {ex.Message}");
            }

        }

        /// <summary>
        /// Kill Player i.e. Post new Kill
        /// </summary>
        /// <param name="killDTO">KillPlayerDTO to be posted</param>
        /// <returns> Kill object wrapped in ActionResult and User </returns>
        /// <response code="200">Succesfull Ok response with entitiy </response>
        /// <response code="400">Server cannot process the request</response>
        /// <response code="403">Access denied</response>
        // POST: api/kills
        [HttpPost]
        public async Task<ActionResult<GetKillDTO>> KillPlayer(KillPlayerDTO killDTO)
        {
            try
            {
                return await _KillService.KillPlayer(User, killDTO);

            }
            catch (NotAuthorizedException ex)
            {
                return Forbid();
            }
            catch (DbUpdateConcurrencyException ex)
            {
                return BadRequest($"Could not kill player: {ex.Message}");
            }
            catch (AppException ex)
            {
                return BadRequest(ex.Message);
            }

        }


    }
}
