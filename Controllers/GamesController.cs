﻿using HVZapi.Helpers;
using HVZapi.Models.Data_Transfer_Objects;
using HVZapi.Models.Domain_Model;
using HVZapi.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HVZapi.Controllers
{
    [Authorize]
    [Produces("application/json")]
    [Route("api/games")]
    [ApiController]
    public class GamesController : Controller
    {
        private readonly IGameService _gameService;
        public GamesController(IGameService gameService)
        {
            _gameService = gameService;
        }

        /// <summary>
        /// Get all Games
        /// </summary>
        /// <returns>A list with Game objects</returns>
        /// <response code="200">Success</response>
        // GET: api/games
        [HttpGet]
        [AllowAnonymous]
        public Task<IEnumerable<GetGameDTO>> Get()
        {
            return _gameService.GetAsync();
        }

        /// <summary>
        /// Get Game by GameId
        /// </summary>
        /// <param name="gameId">Id for specific Game Entity</param>
        /// <response code="200">Success</response>
        /// <response code="404">If no object is returned from database</response>
        /// <returns>A single Game Object</returns>
        // GET: api/games/{gameId}/
        [HttpGet("{gameId}")]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<ActionResult<GetGameDTO>> GetGameById(int gameId)
        {
            try
            {
                return await _gameService.GetGameByIdAsync(gameId);
            }

            catch (AppException ex)
            {
                return NotFound(new { message = ex.Message });
            }
        }

        /// <summary>
        /// Update existing Game Entity
        /// </summary>
        /// <response code="204">Success - No Content</response>
        /// <response code="400">If gameId != gameId from body</response>
        /// <response code="404">If no object is returned from database</response>
        /// <param name="gameId">Id for specific Game Entity</param>
        /// <param name="game">Game Object with updated values</param>
        // PUT: api/games/{gameId}/
        [Authorize(Roles = Role.Admin)]
        [HttpPut("{gameId}")]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public IActionResult UpdateGame(int gameId, PutGameDTO game)
        {
            if (gameId != game.Id)
            {
                return BadRequest();
            }
            try
            {
                _gameService.UpdateGame(game);
                return NoContent();
            }

            catch (AppException ex)
            {
                return NotFound(new { message = ex.Message });
            }

        }

        /// <summary>
        /// Add new Game Entity
        /// </summary>
        /// <param name="gameDTO">New Game Object</param>
        /// <response code="201">Success - No Content</response>
        /// <response code="400">If gameId != gameId from body</response>
        /// <response code="403">Access denied</response>
        /// <returns>Newly created Game Object</returns>
        // POST: api/games/
        [HttpPost]
        [Authorize(Roles = Role.Admin)]
        public async Task<ActionResult<GetGameDTO>> CreateGame(PostGameDTO gameDTO)
        {
            try
            {
                GetGameDTO game = await _gameService.CreateGame(gameDTO);
                return CreatedAtAction("Get", new { id = game.Id }, game);
            }

            catch (AppException ex)
            {
                return BadRequest(new { message = ex.Message });
            }



            // Check for admin, NotAllowed

        }

        /// <summary>
        /// Delete Game Entity by GameId
        /// </summary>
        /// <param name="gameId">Id for specific Game Entity</param>
        /// <response code="200">Succesfull Ok response with entitiy </response>
        /// <response code="404"> Game not Found</response>
        /// <response code="400">Server cannot process the request</response>
        /// <returns>Deleted Game object wrapped in ActionResult</returns>
        // DELETE: api/games/{gameId}/
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [HttpDelete("{gameId}")]
        [Authorize(Roles = Role.Admin)]
        public async Task<ActionResult<GetGameDTO>> DeleteGame(int gameId)
        {
            try
            {
               var game = await _gameService.DeleteGame(gameId);
                return Ok(game);

            }
            catch (AppException ex)
            {
                return NotFound(new { message = ex.Message });
            }
            catch (DbUpdateConcurrencyException ex)
            {
                return BadRequest($"Could not delete game: {ex.Message}");
            }
        }


        /// <summary>
        /// Add new Chat Entity
        /// </summary>
        /// <param name="chatDTO">New Chat Object</param>
        /// <param name="gameId">Id for specific Game Entity</param>
        /// <response code="200">Succesfull Ok response with entitiy </response>
        /// <response code="400">Server cannot process the request</response>
        /// <response code="403">Access denied</response>
        /// <returns>Newly created Chat Object</returns>
        // POST: api/games/{gameId}/chats
        [HttpPost("{gameId}/chats")]
        public async Task<ActionResult<GetChatDTO>> PostGameChat(PostGlobalChatDTO chatDTO,int gameId)
        {
            try
            {
                return await _gameService.AddChatAsync(chatDTO, User);
            }
            catch (NotAuthorizedException)
            {
                return Forbid();
            }
            catch (DbUpdateConcurrencyException ex)
            {
                return BadRequest($"Could not add a new chat: {ex.Message}");
            }
            catch (AppException ex)
            {
                return BadRequest($"Could not add a new chat: {ex.Message}");
            }
        }

        /// <summary>
        /// Get all Chats with specified GameId
        /// </summary>
        /// <param name="gameId">Id for specific Game Entity</param>
        /// <response code="200">Succesfull Ok response with entitiy </response>
        /// <response code="404">Requested resource could not be found</response>
        /// <response code="403">Access denied</response>
        /// <returns>A list with all Chat objects with specific GameId</returns>
        // GET: api/games/{gameId}/chats
        [HttpGet("{gameId}/chats")]
        public async Task<ActionResult<IEnumerable<GetChatDTO>>> GetGameChats(int gameId)
        {
            try
            {
                return Ok(await _gameService.GetGameChats(gameId, User));
            }
            catch (AppException ex)
            {
                return NotFound(new { message = ex.Message });
            }
            catch (NotAuthorizedException)
            {
                return Forbid();
            }
        }

        /// <summary>
        /// Get all Players with specified GameId
        /// </summary>
        /// <param name="gameId">Id for specific Game Entity</param>
        /// <response code="200">Succesfull Ok response with entitiy </response>
        /// <response code="404">Requested resource could not be found </response>
        /// <returns>A list with all Player objects with specific GameId</returns>
        // GET: api/games/{gameId}/players
        [HttpGet("{gameId}/players")]
        public async Task<ActionResult<IEnumerable<GetPlayerWithSquadDTO>>> GetGamePlayers(int gameId)
        {
            try
            {
                return await _gameService.GetGamePlayers(gameId);
            }
            catch (AppException ex)
            {
                return NotFound(new { message = ex.Message });
            }

        }

        /// <summary>
        /// Get all squads with specified gameId
        /// </summary>
        /// <param name="gameId">Id for specific Game Entity</param>
        /// <response code="200">Succesfull Ok response with entitiy </response>
        /// <response code="404">Requested resource could not be found </response>
        /// <returns>A list with all Squad objects with specific GameId</returns>
        // GET: api/games/{gameId}/squads
        [HttpGet("{gameId}/squads")]
        public async Task<ActionResult<IEnumerable<GetSquadAndMembersDTO>>> GetGameSquads(int gameId)
        {
            try
            {
                return await _gameService.GetGameSquads(gameId);
            }
            catch (AppException ex)
            {
                return NotFound(new { message = ex.Message });
            }

        }

        /// <summary>
        /// Get all Missions with specified GameId
        /// </summary>
        /// <param name="gameId">Id for specific Game Entity</param>
        /// <response code="200">Succesfull Ok response with entitiy </response>
        /// <response code="404">Requested resource could not be found </response>
        /// <returns>A list with all Mission objects with specific GameId</returns>
        // GET: api/games/{gameId}/missions
        [HttpGet("{gameId}/missions")]
        public async Task<ActionResult<IEnumerable<GetMissionDTO>>> GetGameMissions(int gameId)
        {
            try
            {
                return await _gameService.GetGameMissions(User,gameId);
            }
            catch (Exception ex)
            {
                return NotFound(new { message = ex.Message });
            }
        }

        /// <summary>
        /// Get all Kills with specified gameId
        /// </summary>
        /// <param name="gameId">Id for specific Game Entity</param>
        /// <response code="200">Succesfull Ok response with entitiy </response>
        /// <response code="404">Requested resource could not be found </response>
        /// <returns>A list with all Kill objects with specific GameId</returns>
        // GET: api/games/{gameId}/kills
        [HttpGet("{gameId}/kills")]
        public async Task<ActionResult<IEnumerable<GetKillDTO>>> GetGameKillsByGameId(int gameId)
        {
            try
            {
                return await _gameService.GetGameKills(gameId);
            }
            catch (AppException ex)
            {
                return NotFound(new { message = ex.Message });
            }
        }

        /// <summary>
        /// Start Game
        /// </summary>
        /// <response code="204">Success - No Content</response>
        /// <response code="400">Bad request</response>
        /// <param name="gameId">Id for specific Game Entity</param>
        /// <returns>Ok - no content</returns>
        // PUT: api/games/{gameId}/start_game
        [HttpPut("{gameId}/start_game")]
        [Authorize(Roles = Role.Admin)]
        public IActionResult StartGame(int gameId)
        {

            try
            {
                _gameService.StartGame(gameId);
                return NoContent();
            }
            catch (AppException ex)
            {
                return BadRequest(new { message = ex.Message });
            }

        }
        /// <summary>
        /// End Game
        /// </summary>
        /// <response code="204">Success - No Content</response>
        /// <response code="400">Bad request</response>
        /// <param name="gameId">Id for specific Game Entity</param>
        /// <returns>Ok - no content</returns>
        // PUT: api/games/{gameId}/start_game
        [HttpPut("{gameId}/end_game")]
        [Authorize(Roles = Role.Admin)]
        public IActionResult EndGame(int gameId)
        {
            try
            {
                _gameService.EndGame(gameId);
                return NoContent();
            }
            catch (AppException ex)
            {
                return BadRequest(new { message = ex.Message });

            }
            catch (DbUpdateConcurrencyException ex)
            {
                return BadRequest(new { message = ex.Message });

            }

        }
        /// <summary>
        /// Join Game
        /// </summary>
        /// <param name="gameId">Id for specific Game Entity</param>
        /// <param name="newPlayer"></param>
        /// <response code="200">Succesfull Ok response with entitiy </response>
        /// <response code="400">Server cannot process the request</response>
        /// <returns>Ok - with object</returns>
        [HttpPost("{gameId}/join_game")]
        public async Task<IActionResult> JoinGame(JoinGameDTO newPlayer,int gameId)
        {
            if(gameId != newPlayer.GameId)
            {
                return BadRequest();
            }
            try
            {
            return Ok(await _gameService.JoinGame(newPlayer));
            }
            catch (AppException ex)
            {

                return BadRequest(new { message = ex.Message});
            }
            catch (DbUpdateConcurrencyException ex)
            {

                return BadRequest(new { message = ex.Message });
            }
        }
    }
}
