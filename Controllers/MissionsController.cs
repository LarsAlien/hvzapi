﻿using HVZapi.Helpers;
using HVZapi.Models.Data_Transfer_Objects;
using HVZapi.Models.Domain_Model;
using HVZapi.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HVZapi.Controllers
{
    [Authorize]
    [Produces("application/json")]
    [Route("api/missions")]
    [ApiController]
    public class MissionsController : Controller
    {
        private readonly IMissionService _missionService;
        public MissionsController(IMissionService missionService)
        {
            _missionService = missionService;
        }

        /// <summary>
        /// Get all Missions
        /// </summary>
        /// <returns>A list with Mission objects</returns>
        /// <response code="200">Succesfull Ok response </response>
        // GET: api/missions
        [ProducesResponseType(StatusCodes.Status200OK)]
        [HttpGet]
        public Task<IEnumerable<GetMissionDTO>> Get()
        {
            return _missionService.GetAllMissionsAsync();
        }

        /// <summary>
        /// Get Mission by MissionId
        /// </summary>
        /// <param name="missionId">Id for specific Mission Entity</param>
        /// <returns>A single Mission Object</returns>
        /// <response code="200">Succesfull Ok response with entitiy </response>
        /// <response code="404">Requested resource could not be found </response>
        // GET: api/missions/{missionId}/
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [HttpGet("{missionId}")]
        public async Task<ActionResult<GetMissionDTO>> GetMissionById(int missionId)
        {
            try
            {
                GetMissionDTO mission = await _missionService.GetMissionByIdAsync(User, missionId);
                return mission;
            }
            catch (NotAuthorizedException ex)
            {
                return Forbid();
            }

            catch (AppException ex)
            {
                return NotFound(new { message = ex.Message });
            }


        }

        /// <summary>
        /// Update existing Mission Entity
        /// </summary>
        /// <param name="missionId">Id for specific Mission Entity</param>
        /// <param name="missionDTO">Mission Object with updated values</param>
        /// <response code="204">Succesfull reponse, no content </response>
        /// <response code="404">Requested resource could not be found </response>
        /// <response code="400">Server cannot process the request</response>
        // PUT: api/missions/{missionId}/
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [Authorize(Roles = Role.Admin)]
        [HttpPut("{missionId}")]
        public IActionResult UpdateMission(int missionId, PutMissionDTO missionDTO)
        {
            if (missionId != missionDTO.Id)
            {
                return BadRequest();
            }
            try
            {
                _missionService.UpdateMission(missionDTO);
                return NoContent();
            }
            catch (AppException ex) {

                return NotFound(new { message = ex.Message });
            }
        }

        /// <summary>
        /// Add new Mission Entity
        /// </summary>
        /// <param name="missionDTO">New Mission Object</param>
        /// <returns>Newly created Mission Object</returns>
        /// <response code="400">Server cannot process the request</response>
        /// <response code="200">Succesfull Ok response with entitiy </response>
        // POST: api/missions/
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [Authorize(Roles = Role.Admin)]
        [HttpPost]
        public async Task<ActionResult<GetMissionDTO>> CreateMission(PostMissionDTO missionDTO)
        {

            try
            {
                GetMissionDTO mission = await _missionService.CreateMission(User, missionDTO);
                return Ok(mission);
            }
            catch (AppException ex)
            {
                return BadRequest(new { message = ex.Message });
            }
            catch (DbUpdateConcurrencyException ex)
            {
                return BadRequest(new { message = ex.Message });
            }


        }

        /// <summary>
        /// Delete Mission Entity by MissionId
        /// </summary>
        /// <param name="missionId">Id for specific Mission Entity</param>
        /// <returns>Deleted Mission object wrapped in ActionResult</returns>
        /// <response code="200">Succesfull Ok response with entitiy </response>
        /// <response code="404">Requested resource could not be found </response>
        // DELETE: api/missions/{missionId}/
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [Authorize(Roles = Role.Admin)]
        [HttpDelete("{missionId}")]
        public async Task<ActionResult<GetMissionDTO>> Delete(int missionId)
        {
            try
            {
                var mission = await _missionService.DeleteMission(missionId);
                return Ok(mission);

            }
            catch (AppException ex)
            {
                return NotFound(new { message = ex.Message });
            }


        }

        /// <summary>
        /// Get all Missions
        /// </summary>
        /// <returns>A list with Mission objects</returns>
        /// <response code="200">Succesfull Ok response </response>
        // GET: api/missions
        [ProducesResponseType(StatusCodes.Status200OK)]
        [HttpGet("{gameId}")]
        public Task<IEnumerable<GetMissionDTO>> GetAllFactionMissions(int gameId)
        {
            return _missionService.GetAllFactionMissions(User, gameId);
        }


    }
}
