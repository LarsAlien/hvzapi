﻿using HVZapi.Helpers;
using HVZapi.Models.Data_Transfer_Objects;
using HVZapi.Models.Domain_Model;
using HVZapi.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HVZapi.Controllers
{
    [Authorize]
    [Produces("application/json")]
    [Route("api/players")]
    [ApiController]
    public class PlayersController : Controller
    {
        private IPlayerService _playerService;

        public PlayersController(IPlayerService playerService)
        {
            _playerService = playerService;
        }

        /// <summary>
        /// Get all Players
        /// </summary>
        /// <returns>A list with Player objects</returns>
        /// <response code="200">Succesfull Ok response </response>
        //GET: api/players
        [ProducesResponseType(StatusCodes.Status200OK)]
        [HttpGet]
        public Task<IEnumerable<GetPlayerDTO>> GetPlayers()
        {
            return _playerService.GetPlayers();
        }

        /// <summary>
        /// Get Player by PlayerId
        /// </summary>
        /// <param name="playerId">Id for specific Player Entity</param>
        /// <returns>A single Player Object</returns>
        /// <response code="200">Succesfull Ok response with entitiy </response>
        /// <response code="404">Requested resource could not be found </response>
        ///         [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [HttpGet("{playerId}")]
        public async Task<ActionResult<GetPlayerDTO>> GetPlayer(int playerId)
        {
            try
            {
                GetPlayerDTO player = await _playerService.GetPlayer(playerId);
                return player;
            }
           
            catch(AppException ex)
            {
                return NotFound(ex.Message);
            }
                  
        }

        /// <summary>
        /// Update existing Player Entity
        /// </summary>
        /// <param name="playerId"></param>
        /// <param name="player"></param>
        /// <response code="204">Succesfull reponse, no content </response>
        /// <response code="404">Requested resource could not be found </response>
        /// <response code="400">Server cannot process the request</response>
        // PUT: api/players/{playerId}
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [HttpPut("{playerId}")]
        [Authorize(Roles = Role.Admin)]
        public IActionResult  UpdatePlayer(int playerId, GetPlayerDTO player)
        {
            if (playerId != player.Id)
            {
                return BadRequest();
            }
            try
            {
                 _playerService.UpdatePlayer(player);
                return NoContent();
            }
            catch(AppException ex)
            {
                return NotFound(new {message = ex.Message});
            }
         
            
        }

        /// <summary>
        /// Add new Player entity
        /// </summary>
        /// <param name="playerDTO"></param>
        /// <returns>Newly created Player object</returns>
        /// <response code="400">Server cannot process the request</response>
        /// <response code="200">Succesfull Ok response with entitiy </response>
        // POST: api/players
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [HttpPost]
        public async Task<ActionResult<GetPlayerDTO>> CreatePlayer(PostPlayerDTO playerDTO)
        {
            try
            {
                GetPlayerDTO player = await _playerService.CreatePlayer(playerDTO);
                return Ok(player);
            }
            
           catch(AppException ex)
            {
                return BadRequest(new { message = ex.Message});
            }
               
            
            // Check for admin, NotAllowed
            
        }

        /// <summary>
        /// Delete Player Entity by PlayerId
        /// </summary>
        /// <param name="playerId"></param>
        /// <returns>Deleted Player object wrapped in ActionResult</returns>
        /// <response code="200">Succesfull Ok response with entitiy </response>
        /// <response code="404">Requested resource could not be found </response>
        // DELETE: api/players/{playerId}
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [HttpDelete("{playerId}")]
        [Authorize(Roles = Role.Admin)]
        public async Task<ActionResult<GetPlayerDTO>> Delete(int playerId)
        {
            try
            {
                var player = await _playerService.DeletePlayer(playerId);
                return Ok(player);
            }
           
            catch(AppException ex)
            {
                return NotFound(new { message = ex.Message});

            }
                
            }
            // Check for admin, NotAuthorized
       
        }



    }

