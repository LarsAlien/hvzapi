﻿using AutoMapper;
using HVZapi.Helpers;
using HVZapi.Models;
using HVZapi.Models.Data_Transfer_Objects;
using HVZapi.Models.Domain_Model;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace HVZapi.Services
{
    /// <summary>
    ///  MissionService  class.
    /// Include all service for Mission table
    /// </summary>
    public class MissionService : IMissionService
    {
        private readonly GameDbContext _context;
        private readonly IMapper _mapper;
        public MissionService(GameDbContext context, IMapper mapper)
        {
            _mapper = mapper;
            _context = context;
        }


        //Get all missions
        public async Task<IEnumerable<GetMissionDTO>> GetAllMissionsAsync()
        {
            return _mapper.Map<List<GetMissionDTO>>(await _context.Missions.ToListAsync());
        }

        //Get all FactionsMissions
        public async Task<IEnumerable<GetMissionDTO>> GetAllFactionMissions(ClaimsPrincipal user, int gameId)
        {
            // get Current User
            int currentUserId = int.Parse(user.Identity.Name);
            Player player = await _context.Players
                                   .SingleAsync(p => p.UserId == currentUserId && p.GameId == gameId);


            return _mapper.Map<List<GetMissionDTO>>(await _context.Missions
                                        .Where(m => m.IsHumanVisible == player.IsHuman && m.GameId == player.GameId)
                                        .ToListAsync());
        }

        //Get a Mission to user by id
        public async Task<GetMissionDTO> GetMissionByIdAsync(ClaimsPrincipal user, int missionId)
        {
            var mission = await _context.Missions.FindAsync(missionId);
            if(mission == null)
                throw new AppException($"The Mission you are tying to get with Id: {missionId} doesn't exist ");


            int currentUserId = int.Parse(user.Identity.Name);
            Player player = await _context.Players.
                                SingleAsync(p => p.UserId == currentUserId && p.GameId == mission.GameId);

            if( mission.IsHumanVisible != player.IsHuman && !user.IsInRole(Role.Admin))
            {
                throw new NotAuthorizedException("You don't belong to this mission's faction");
            }


            return _mapper.Map<GetMissionDTO>(await _context.Missions.FindAsync(missionId));
        }

        //Create a Mission to user 
        public async Task<GetMissionDTO> CreateMission(ClaimsPrincipal user, PostMissionDTO missionDTO)
        {
            Mission mission = _mapper.Map<Mission>(missionDTO);

            // add mission to DB and return mission object
            _context.Missions.Add(mission);
            await _context.SaveChangesAsync();
            return _mapper.Map<GetMissionDTO>(mission);
        }

        //Delete a Mission to user 
        public async Task<GetMissionDTO> DeleteMission(int missionId)
        {
            Mission mission = await _context.Missions.FindAsync(missionId);
            //if the mission not found 
            if(mission == null)
                throw new AppException($"The mission you are trying to delete with {missionId} is not found");
            _context.Missions.Remove(mission);
            await _context.SaveChangesAsync();
            return _mapper.Map<GetMissionDTO>(mission);
        }

        //Update a Mission to user 
        public void UpdateMission(PutMissionDTO missionDTO)
        {

            bool find = _context.Missions.Any(c => c.Id == missionDTO.Id);
           
            if (!find)
                throw new AppException($"Error: Mission you are trying to update with id: {missionDTO.Id} is not found");

            Mission mission = _mapper.Map<Mission>(missionDTO);

            try
            {
                _context.Entry(mission).State = EntityState.Modified;
                 _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException ex)
            {
                Console.WriteLine(ex);
            }

        }
    }
}
