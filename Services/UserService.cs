using AutoMapper;
using HVZapi.Helpers;
using HVZapi.Models;
using HVZapi.Models.Data_Transfer_Objects;
using HVZapi.Models.Domain_Model;
using HVZapi.Services;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HVZapi.Services
{
    /// <summary>
    ///  UserService class.
    /// Include all service for User table
    /// </summary>
    public class UserService : IUserService
    {
        private readonly IMapper _mapper;
        private GameDbContext _context;
        public UserService(GameDbContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        public User AuthenticateUser(string username, string password)
        {
            if (string.IsNullOrEmpty(username) || string.IsNullOrEmpty(password))
                return null;

            var user = _context.Users.SingleOrDefault(x => x.Username == username);

            // check if username exists
            if (user == null)
                return null;

            // check if password is correct
            if (!VerifyPasswordHash(password, user.PasswordHash, user.PasswordSalt))
                return null;

            // authentication successful
            return user;
        }

        public IEnumerable<User> GetAllUser()
        {
            return _context.Users;
        }

        public User GetUserById(int id)
        {
            return _context.Users.Find(id);
        }

        public User CreateUser(User user, string password)
        {
            // validation
            if (string.IsNullOrWhiteSpace(password))
                throw new AppException("Password is required");

            if (_context.Users.Any(x => x.Username == user.Username))
                throw new AppException("Username \"" + user.Username + "\" is already taken");

            byte[] passwordHash, passwordSalt;
            CreatePasswordHash(password, out passwordHash, out passwordSalt);

            user.PasswordHash = passwordHash;
            user.PasswordSalt = passwordSalt;
            user.Gender = "";

            _context.Users.Add(user);
            _context.SaveChanges();

            return user;
        }

        public void UpdateUser(User userToUpdate, string password = null)
        {
            var user = _context.Users.Find(userToUpdate.Id);

            if (user == null)
                throw new AppException("User not found");

            // update username if it has changed
            if (!string.IsNullOrWhiteSpace(userToUpdate.Username) && userToUpdate.Username != user.Username)
            {
                // throw error if the new username is already taken
                if (_context.Users.Any(x => x.Username == userToUpdate.Username))
                    throw new AppException("Username " + userToUpdate.Username + " is already taken");

                user.Username = userToUpdate.Username;
            }


            if (!string.IsNullOrWhiteSpace(userToUpdate.Email) && userToUpdate.Email != user.Email)
            {
                // throw error if the new Email is already taken
                if (_context.Users.Any(x => x.Email == userToUpdate.Email))
                    throw new AppException("Email " + userToUpdate.Email + " is already taken");

                user.Email = userToUpdate.Email;
            }





            // update user properties if provided
            if (!string.IsNullOrWhiteSpace(userToUpdate.FirstName))
                user.FirstName = userToUpdate.FirstName;

            if (!string.IsNullOrWhiteSpace(userToUpdate.LastName))
                user.LastName = userToUpdate.LastName;

            if (!string.IsNullOrWhiteSpace(userToUpdate.Phone))
                user.Phone = userToUpdate.Phone;

            if (!string.IsNullOrWhiteSpace(userToUpdate.Gender))
                user.Gender = userToUpdate.Gender;
            if(userToUpdate.Age>0 && userToUpdate.Age<120)
                    user.Age = userToUpdate.Age;



            // update password if provided
            if (!string.IsNullOrWhiteSpace(password))
            {
                byte[] passwordHash, passwordSalt;
                CreatePasswordHash(password, out passwordHash, out passwordSalt);

                user.PasswordHash = passwordHash;
                user.PasswordSalt = passwordSalt;
            }

            _context.Users.Update(user);
            _context.SaveChanges();
        }

        public void DeleteUserById(int id)
        {
            var user = _context.Users.Find(id);
            if(user == null)
            {
                throw new AppException("The user you are tying to delete doesn't exist ");
            }

                _context.Users.Remove(user);
                _context.SaveChanges();

        }

        // private helper methods

        public static void CreatePasswordHash(string password, out byte[] passwordHash, out byte[] passwordSalt)
        {
            if (password == null) throw new ArgumentNullException("password");
            if (string.IsNullOrWhiteSpace(password)) throw new ArgumentException("Value cannot be empty or whitespace only string.", "password");

            using (var hmac = new System.Security.Cryptography.HMACSHA512())
            {
                passwordSalt = hmac.Key;
                passwordHash = hmac.ComputeHash(System.Text.Encoding.UTF8.GetBytes(password));
            }
        }

        private static bool VerifyPasswordHash(string password, byte[] storedHash, byte[] storedSalt)
        {
            if (password == null) throw new ArgumentNullException("password");
            if (string.IsNullOrWhiteSpace(password)) throw new ArgumentException("Value cannot be empty or whitespace only string.", "password");
            if (storedHash.Length != 64) throw new ArgumentException("Invalid length of password hash (64 bytes expected).", "passwordHash");
            if (storedSalt.Length != 128) throw new ArgumentException("Invalid length of password salt (128 bytes expected).", "passwordHash");

            using (var hmac = new System.Security.Cryptography.HMACSHA512(storedSalt))
            {
                var computedHash = hmac.ComputeHash(System.Text.Encoding.UTF8.GetBytes(password));
                for (int i = 0; i < computedHash.Length; i++)
                {
                    if (computedHash[i] != storedHash[i]) return false;
                }
            }

            return true;
        }

        public async Task<ActionResult<IEnumerable<GetPlayerWithAllStatsDTO>>> GetUserPlayers(int userId)
        {
            User user = await _context.Users.FindAsync(userId);
            if (user == null)
                throw new AppException($"The user you are trying to access does not exist");

            IEnumerable<Player> players = _context.Players
                                                .Include(p => p.Game)
                                                .Include(p => p.KillerKills)
                                                .Include(p => p.VictimKills)
                                                .Include(p => p.SquadMember).ThenInclude(sm => sm.Squad)
                                                .Where(p => p.UserId == userId);
            return _mapper.Map<List<GetPlayerWithAllStatsDTO>>(players);
        }
    }
}

