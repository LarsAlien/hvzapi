﻿using HVZapi.Models.Data_Transfer_Objects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace HVZapi.Services
{
    public interface IKillService
    {
        //Get a list of kills
        public Task<IEnumerable<GetKillDTO>> GetAllKillsAsync();
        //Returns a specific kill object
        public Task<GetKillDTO> GetKillByIdAsync(int id);
        // Updates a kill object
        public void UpdateKill(ClaimsPrincipal user, GetKillDTO kill);

        //Delete a kill. Admin only.
        public Task<GetKillDTO> Delete(int id);
        //Creates a kill object by looking up the victim by the specified bite code
        Task<GetKillDTO> KillPlayer(ClaimsPrincipal user, KillPlayerDTO killDTO);
    }
}
