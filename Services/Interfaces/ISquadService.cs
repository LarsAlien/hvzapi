﻿using HVZapi.Models.Data_Transfer_Objects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace HVZapi.Services
{
    public interface ISquadService
    {
        public Task<IEnumerable<GetSquadDTO>> GetSquads();
        public Task<GetSquadAndMembersDTO> GetSquad(int squadId, ClaimsPrincipal user);
        public void UpdateSquad(GetSquadDTO squadDTO);
        public Task<GetSquadDTO> CreateSquad(PostSquadDTO squadDTO, ClaimsPrincipal user);
        public void DeleteSquad(int id);
        public void LeaveSquad(int squadId, ClaimsPrincipal user);
        //Chat
        public Task<List<GetChatDTO>> GetChat(int squadId, ClaimsPrincipal user);
        public Task<GetChatDTO> PostChat(PostSquadChatDTO chat, ClaimsPrincipal user);
        Task<List<GetCheckinDTO>> GetCheckin(int squadId, ClaimsPrincipal user);
        Task<GetCheckinDTO> PostCheckin(PostCheckinDTO checkinDTO, ClaimsPrincipal user);
        Task<GetSquadMemberDTO> JoinSquad(PostSquadMemberDTO squadMemberDTO);
    }
}
