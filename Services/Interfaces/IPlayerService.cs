﻿using HVZapi.Models.Data_Transfer_Objects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HVZapi.Services
{
    public interface IPlayerService
    {
        public Task<IEnumerable<GetPlayerDTO>> GetPlayers();

        public Task<GetPlayerDTO> GetPlayer(int playerId);
        public void UpdatePlayer(GetPlayerDTO playerDTO);
        public Task<GetPlayerDTO> CreatePlayer(PostPlayerDTO playerDTO);
        public Task<GetPlayerDTO> DeletePlayer(int id);
        public Task<GetPlayerDTO> SearchPlayer(string name);
    }
}
