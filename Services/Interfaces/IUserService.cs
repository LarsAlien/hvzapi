﻿using HVZapi.Models.Data_Transfer_Objects;
using HVZapi.Models.Domain_Model;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace HVZapi.Services
{

        public interface IUserService
        {
          public  User AuthenticateUser(string username, string password);
         public   IEnumerable<User> GetAllUser();
        public    User GetUserById(int id);
         public  User CreateUser(User user, string password);
         public   void UpdateUser(User user, string password = null);
          public  void DeleteUserById(int id);
        Task<ActionResult<IEnumerable<GetPlayerWithAllStatsDTO>>> GetUserPlayers(int userId);
    }
    }
