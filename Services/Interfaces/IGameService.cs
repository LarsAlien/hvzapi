﻿using HVZapi.Models.Data_Transfer_Objects;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace HVZapi.Services
{
    public interface IGameService
    {
        public Task<IEnumerable<GetGameDTO>> GetAsync();

        public Task<GetGameDTO> GetGameByIdAsync(int id);

        public void UpdateGame(PutGameDTO game);

        public Task<GetGameDTO> CreateGame(PostGameDTO game);

        public Task<GetGameDTO> DeleteGame(int id);
        public Task<GetChatDTO> AddChatAsync(PostGlobalChatDTO chatDTO, ClaimsPrincipal user);
        public Task<IEnumerable<GetChatDTO>> GetGameChats(int gameId, ClaimsPrincipal user);
        Task<ActionResult<IEnumerable<GetPlayerWithSquadDTO>>> GetGamePlayers(int gameId);
        Task<ActionResult<IEnumerable<GetSquadAndMembersDTO>>> GetGameSquads(int gameId);
        Task<ActionResult<IEnumerable<GetMissionDTO>>> GetGameMissions(ClaimsPrincipal user,int gameId);
        Task<ActionResult<IEnumerable<GetKillDTO>>> GetGameKills(int gameId);
        public void StartGame(int gameId);
        public void EndGame(int gameId);
        public Task<GetPlayerDTO> JoinGame(JoinGameDTO game);
    }
}
