﻿using HVZapi.Models.Data_Transfer_Objects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace HVZapi.Services
{
   public  interface IMissionService
    {
        //Get a list of Missions
        public Task<IEnumerable<GetMissionDTO>> GetAllMissionsAsync();
        //Returns a specific Mission object
        public Task<GetMissionDTO> GetMissionByIdAsync(ClaimsPrincipal user, int missionId);
        // Updates a Mission object
        public void UpdateMission(PutMissionDTO mission);
        //Creates a Mission object
        public Task<GetMissionDTO> CreateMission(ClaimsPrincipal user, PostMissionDTO mission);

        //Delete a Mission. Admin only.
        public Task<GetMissionDTO> DeleteMission(int missionId);

        public Task<IEnumerable<GetMissionDTO>> GetAllFactionMissions(ClaimsPrincipal user, int gameId);
    }
}
