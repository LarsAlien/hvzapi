﻿using AutoMapper;
using HVZapi.Helpers;
using HVZapi.Models;
using HVZapi.Models.Data_Transfer_Objects;
using HVZapi.Models.Domain_Model;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HVZapi.Services
{
    /// <summary>
    ///  PlayerService class.
    /// Include all service for Player table
    /// </summary>
    public class PlayerService : IPlayerService
    {
        private readonly GameDbContext _context;
        private readonly IMapper _mapper;

        public PlayerService(GameDbContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        // Get all players
        public async Task<IEnumerable<GetPlayerDTO>> GetPlayers()
        {

            return _mapper.Map<List<GetPlayerDTO>>(await _context.Players.Include(p => p.User).ToListAsync());
        }


        // Get a player by id 
        public async Task<GetPlayerDTO> GetPlayer(int playerId)

        {
            var player = await _context.Players.FindAsync(playerId);
            if (player== null)
                throw new AppException($"The Player you are tying to get with Id: {playerId} doesn't exist ");

            return _mapper.Map<GetPlayerDTO>(await _context.Players.Include(p => p.User).SingleAsync(s => s.Id == playerId));
        }

        // Create a new player
        public async Task<GetPlayerDTO> CreatePlayer(PostPlayerDTO playerDTO)
        {
            var playerTofind = _context.Players.Where(x => x.GameId == playerDTO.GameId && x.UserId == playerDTO.UserId).FirstOrDefault();

            if( playerTofind!= null)
                 throw new AppException($"The player allready joined this game");


            Player player = _mapper.Map<Player>(playerDTO);

            if(player== null)
                throw new AppException($"Could not create a new player");


            player.BiteCode = BiteCodeGenerator.Generator();
            _context.Players.Add(player);
            await _context.SaveChangesAsync();
            return _mapper.Map<GetPlayerDTO>(player);
        }

        // Update a player
        public void UpdatePlayer(GetPlayerDTO playerDTO)
        {
            bool find = _context.Players.Any(c => c.Id == playerDTO.Id);

            // if player is not found
            if (!find)
                throw new AppException($"Player you are trying to update with id: {playerDTO.Id} is not found");



            Player player = _mapper.Map<Player>(playerDTO);
            try
            {
                _context.Entry(player).State = EntityState.Modified;
                 _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException ex)
            {
                Console.WriteLine(ex);
            
            }
           
        }

        // Delete a player
        public async Task<GetPlayerDTO> DeletePlayer(int id)
        {
            var player = await _context.Players.FindAsync(id);
            if(player == null)
                throw new AppException($"The player you are trying to delete with {id} is not found");
            _context.Players.Remove(player);
            await _context.SaveChangesAsync();

            return _mapper.Map<GetPlayerDTO>(player);
        }

        // Search for a player
        public Task<GetPlayerDTO> SearchPlayer(string name)
        {

            //TODO
            throw new NotImplementedException();
        }


    }
}
