﻿using AutoMapper;
using HVZapi.Models;
using HVZapi.Models.Data_Transfer_Objects;
using HVZapi.Models.Domain_Model;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Authorization;
using System.Threading.Tasks;
using HVZapi.Helpers;
using Microsoft.AspNetCore.Mvc;
using System.Security.Claims;

namespace HVZapi.Services
{
    /// <summary>
    ///  KillService class.
    /// Include all service for Kill table
    /// </summary>
    public class KillService : IKillService
    {
        private readonly GameDbContext _context;
        private readonly IMapper _mapper;
        public KillService(GameDbContext context, IMapper mapper)
        {
            _mapper = mapper;
            _context = context;
        }

        // Get all Kills
        public async Task<IEnumerable<GetKillDTO>> GetAllKillsAsync()
        {
            return _mapper.Map<List<GetKillDTO>>(await _context.Kills.ToListAsync());
        }

        // Get Kill by Id
        public async Task<GetKillDTO> GetKillByIdAsync(int killId)
        {   
            // Check if kill exist
            var kill = await _context.Kills.FindAsync(killId);
            if(kill == null)
                throw new AppException($"Error: The kill you are trying to get with id: {killId} is not exist");

            return _mapper.Map<GetKillDTO>(kill);
        }

        // Update existing Game
        public void UpdateKill(ClaimsPrincipal user, GetKillDTO killDTO)
        {
            // Check if kill exist
            bool find = _context.Kills.Any(k => k.Id == killDTO.Id);
            if (!find)
                throw new AppException($"Error: Kill you are trying to update with KillId: {killDTO.Id} is not found");

            // Set access to user and admin only
            Player killer =  _context.Players
                               .Include(s => s.User)
                               .Single(p => p.Id == killDTO.KillerId);
            var currentUserId = int.Parse(user.Identity.Name);
            if (killer.User.Id != currentUserId && !user.IsInRole(Role.Admin))
                throw new NotAuthorizedException("User access was denied");

            // Map and save Kill
            Kill kill = _mapper.Map<Kill>(killDTO);

            try
            {
                _context.Entry(kill).State = EntityState.Modified;
                 _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException ex)
            {
                Console.WriteLine(ex);
            }

        }

        // Delete Kill
        public async Task<GetKillDTO> Delete(int killId)
        {
            // Check if Kill exist
            Kill kill = await _context.Kills.FindAsync(killId);
            if (kill == null)
                throw new AppException("The kill does not exist");

            // Map, save and return Kill
            _context.Kills.Remove(kill);
            await _context.SaveChangesAsync();
            return _mapper.Map<GetKillDTO>(kill);
        }

        // Kill player (Post new Kill)
        public async Task<GetKillDTO> KillPlayer(ClaimsPrincipal user, KillPlayerDTO killDTO)
        {
            Player killer = await _context.Players
                               .Include(s => s.User)
                               .SingleAsync(p => p.Id == killDTO.KillerId);

            // Set access to user and admin only
            var currentUserId = int.Parse(user.Identity.Name);
            if (killer.User.Id != currentUserId && !user.IsInRole(Role.Admin))
                    throw new NotAuthorizedException("User access was denied");
            Player victim;
            try
            {
                victim = await _context.Players
                   .SingleAsync(p => p.BiteCode == killDTO.BiteCode);
            }
            catch (Exception ex)
            {

                throw new AppException($"Provided Bitecode don't match any Bitecode in database{ex.Message}");
            }

            // iF victim dont exist, return null
            if (victim == null) throw new AppException("victim with bite code dont exist");
            // if victim is already a zombie, throw exception
            if (victim.IsHuman == false)
            {
                throw new AppException("Player is already zombie");
            }
            // if
            if (victim.GameId != killer.GameId && !user.IsInRole(Role.Admin) )
            {
                throw new NotAuthorizedException("Cannot kill a player in a different game than you");
            }
            //set victim to zombie
            victim.IsHuman = false;

            // new Kill object
            Kill kill = new Kill()
            {
                GameId = killDTO.GameId,
                TOD = killDTO.TOD,
                IsVisible = killDTO.IsVisible,
                KillerID = killDTO.KillerId,
                Lat = killDTO.lat,
                Lng = killDTO.lng,
                VictimID = victim.Id,
                Story = killDTO.Story,
            };


            // Map, save and reutrn Kill
            _context.Kills.Add(kill);
            _context.Entry(victim).State = EntityState.Modified;
            await _context.SaveChangesAsync();

            var getKill = _mapper.Map<GetKillDTO>(kill);
            return getKill;
        }
    }
}
