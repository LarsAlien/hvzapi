﻿using AutoMapper;
using HVZapi.Helpers;
using HVZapi.Models;
using HVZapi.Models.Data_Transfer_Objects;
using HVZapi.Models.Domain_Model;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace HVZapi.Services
{
    /// <summary>
    ///  GameService class.
    /// Include all service for game table
    /// </summary>
    public class GameService : IGameService
    {
        private readonly GameDbContext _context;
        private readonly IMapper _mapper;
        public GameService(GameDbContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        // Get all Games
        public async Task<IEnumerable<GetGameDTO>> GetAsync()
        {
            return _mapper.Map<List<GetGameDTO>>(await _context.Games.Include(g => g.Players).ThenInclude(p => p.User).ToListAsync());
        }

        // Get Game på Id
        public async Task<GetGameDTO> GetGameByIdAsync(int id)
        {
            //Check if Game exist
            var game = await _context.Games.Include(g => g.Players).SingleAsync(g => g.Id == id);
            if (game == null)
                throw new AppException($"Error: The game you are trying to get with id: {id} is not exist");

            return _mapper.Map<GetGameDTO>(game);
        }

        // Create new Game
        public async Task<GetGameDTO> CreateGame(PostGameDTO gameDTO)
        {
            // Map, save and return new Game
            Game game = _mapper.Map<Game>(gameDTO);
            _context.Games.Add(game);
            await _context.SaveChangesAsync();
            return _mapper.Map<GetGameDTO>(game);
        }

        // Update existing Game
        public void UpdateGame(PutGameDTO gameDTO)
        {
            //Check if Game exist
            bool find = _context.Games.Any(c => c.Id == gameDTO.Id);
            if (!find)
                throw new AppException($"Error: Game you are trying to update with gameid: {gameDTO.Id} is not found");
           
            // Map and save changes
            Game gameToUpdate = _mapper.Map<Game>(gameDTO);
            try
            {
                _context.Entry(gameToUpdate).State = EntityState.Modified;
                _context.SaveChanges();
            }
            catch (DbUpdateConcurrencyException ex)
            {
                Console.WriteLine(ex);

            }
        }

        // Delete Game
        public async Task<GetGameDTO> DeleteGame(int id)
        {
            // Check if Game exist
            Game game = await _context.Games.FindAsync(id);
            if (game == null)
                throw new AppException("The game does not exist");

            //Save changes and return Game
            _context.Games.Remove(game);
            await _context.SaveChangesAsync();
            return _mapper.Map<GetGameDTO>(game);
        }

        // Add new Game Chat
        public async Task<GetChatDTO> AddChatAsync(PostGlobalChatDTO chatDTO, ClaimsPrincipal user)
        {
            // Check if player is in game
            Player player = await IsPlayer(user, chatDTO.GameId);
            if (player == null) throw new AppException("Player does not exist");
            
            //new Chat object
            Chat chat = new Chat()
            {
                Message = chatDTO.Message,
                Player = player,
                GameId = chatDTO.GameId,
                ChatTime = DateTime.Now,
                IsHumanGlobal = !chatDTO.IsGlobal && player.IsHuman,
                IsZombieGlobal = !chatDTO.IsGlobal && !player.IsHuman,
            };

            // Map, save and return new game Chat
            _context.Chats.Add(chat);
            await _context.SaveChangesAsync();
            return _mapper.Map<GetChatDTO>(chat);
        }



        public async Task<IEnumerable<GetChatDTO>> GetGameChats(int gameId, ClaimsPrincipal user)
        {
            Game game = await _context.Games.FindAsync(gameId);

            // Check if player is in game
            Player player = await IsPlayer(user, gameId);
            if (game == null)
                throw new AppException($"The chats you are tying to get with gameId: {gameId} doesn't exist ");
            
            // Get all chats if user is Admin
            var chatlist = user.IsInRole(Role.Admin)
             ? await _context.Chats
                .Include(sm => sm.Player).ThenInclude(p => p.User)
                .Where(c => c.GameId == gameId).ToListAsync()

            // Return appropriate chats based on player faction
            : await _context.Chats
                .Include(sm => sm.Player).ThenInclude(p => p.User)
                .Where(c => c.GameId == gameId && player.IsHuman
                ? (c.IsHumanGlobal && !c.IsZombieGlobal) || (!c.IsHumanGlobal && !c.IsZombieGlobal)
                : (!c.IsHumanGlobal && c.IsZombieGlobal) || (!c.IsHumanGlobal && !c.IsZombieGlobal)
                ).ToListAsync();

            return _mapper.Map<List<GetChatDTO>>(chatlist);
        }

        // Get players by Game with Squad members
        public async Task<ActionResult<IEnumerable<GetPlayerWithSquadDTO>>> GetGamePlayers(int gameId)
        {
            // Check if game exist
            Game game = await _context.Games.SingleAsync(g => g.Id == gameId);
            if (game == null)
                throw new AppException($"The players you are tying to get with gameId: {gameId} doesn't exist ");

            // Instanciate List of Squads, including squad members
            List<Player> players = await _context.Players.Include(p => p.User)
                .Include(p => p.SquadMember).ThenInclude(sm => sm.Squad).ThenInclude(s => s.SquadMembers)
                .Include(p => p.SquadMember).ThenInclude(s => s.Squad).ThenInclude(sq => sq.SquadCheckIns)
                .Where(p => p.GameId == gameId).ToListAsync();
            return _mapper.Map<List<GetPlayerWithSquadDTO>>(players);
        }

        // Get all Squads by Game
        public async Task<ActionResult<IEnumerable<GetSquadAndMembersDTO>>> GetGameSquads(int gameId)
        {
            // Check if Game exist
            Game game = await _context.Games.FindAsync(gameId);
            if (game == null)
                throw new AppException($"The Squads you are tying to get with gameId: {gameId} doesn't exist ");

            // Instanciate List of Squads, including squad members
            List<Squad> squadList = await _context.Squads
                .Include(s => s.SquadMembers).ThenInclude(sm => sm.Player).ThenInclude(p => p.User)
                .Where(s => s.GameId == gameId).ToListAsync();
            return _mapper.Map<List<GetSquadAndMembersDTO>>(squadList);
        }

        // Get Missions by Game
        public async Task<ActionResult<IEnumerable<GetMissionDTO>>> GetGameMissions(ClaimsPrincipal user, int gameId)
        {
            // Check if Game exist
            Game game = await _context.Games.FindAsync(gameId);
            if (game == null)
                throw new AppException($"The Missions you are tying to get with gameId: {gameId} doesn't exist ");
            //Player player = await IsPlayer(user, gameId);
            Player player = await _context.Players.SingleAsync(p => p.UserId == int.Parse(user.Identity.Name) && p.GameId == gameId);

            List<Mission> missionList = await _context.Missions.Where(m => m.GameId == gameId).ToListAsync();

            return _mapper.Map<List<GetMissionDTO>>(missionList);
        }

        // Get Kills by Game
        public async Task<ActionResult<IEnumerable<GetKillDTO>>> GetGameKills(int gameId)
        {
            // Check if Game exist
            Game game = await _context.Games.FindAsync(gameId);
            if (game == null)
                throw new AppException($"The Kills you are tying to get with gameId: {gameId} doesn't exist ");
            
            var gameList = await _context.Kills.Where(s => s.GameId == gameId).ToListAsync();
            return _mapper.Map<List<GetKillDTO>>(gameList);
        }

        // Start Game
        public void StartGame(int gameId)
        {
            Game gameToUpdate = _context.Games.Find(gameId);
            if (gameToUpdate.GameStarted == true)
            {
                throw new AppException($"The game is already started");
            }

            // Get all players in game
            var playerlist = _context.Players.Where(p => p.GameId == gameToUpdate.Id).ToList();

            if(playerlist.Count == 0) throw new AppException($"This game has no registered players");

            //Select "patient zero"
            var random = new Random();
            var index = random.Next(playerlist.Count);
            var patientZero = playerlist[index];
            patientZero.IsPatientZero = true;
            patientZero.IsHuman = false;

            //Set appropriate game settings on start
            gameToUpdate.GameStarted = true;
            gameToUpdate.RegistrationOpen = false;
            gameToUpdate.GameComplete = false;
            gameToUpdate.StartTime = DateTime.Now;

            // Save changes
            try
            {
                _context.Entry(gameToUpdate).State = EntityState.Modified;
                _context.Entry(patientZero).State = EntityState.Modified;
                _context.SaveChanges();
            }
            catch (DbUpdateConcurrencyException ex)
            {
                Console.WriteLine(ex);

            }
        }

        // End game
        public void EndGame(int gameId)
        {
            // Check Game status
            Game gameToUpdate = _context.Games.Find(gameId);
            if (gameToUpdate.GameComplete)
            {
                throw new AppException($"The game has already ended");
            }


            //Set appropriate game settings on start
            gameToUpdate.GameStarted = false;
            gameToUpdate.RegistrationOpen = false;
            gameToUpdate.GameComplete = true;
            gameToUpdate.EndTime = DateTime.Now;

            // Save changes
            _context.Entry(gameToUpdate).State = EntityState.Modified;
            _context.SaveChanges();
        }

        // Join Game
        public async Task<GetPlayerDTO> JoinGame(JoinGameDTO newPlayer)
        {
            var playerTofind = _context.Players.Where(x => x.GameId == newPlayer.GameId && x.UserId == newPlayer.UserId).FirstOrDefault();
            if (playerTofind != null)
                throw new AppException($"The player allready joined this game");

            // New player object
            Player player = new Player()
            {
                UserId = newPlayer.UserId,
                GameId = newPlayer.GameId,
                IsHuman = true,
                IsPatientZero = false,
                BiteCode = BiteCodeGenerator.Generator()
            };

            // Save changes, map and return Player
            await _context.Players.AddAsync(player);
            await _context.SaveChangesAsync();
            return _mapper.Map<GetPlayerDTO>(player);
        }

        // Method used to chekc if Player is in game or is Admin
        private async Task<Player> IsPlayer(ClaimsPrincipal user, int gameId)
        {
            // Set access to user and admin only
            Player player;
            var currentUserId = int.Parse(user.Identity.Name);
            bool found = _context.Players.Include(p => p.User)
                                        .Any(p => p.UserId == currentUserId && p.GameId == gameId);

            if (!found && !user.IsInRole(Role.Admin))
                throw new NotAuthorizedException();
            else if (user.IsInRole(Role.Admin)) return null;
            else
                player = await _context.Players.Include(p => p.User)
                                        .SingleOrDefaultAsync(p => p.UserId == currentUserId && p.GameId == gameId);

            return player;
        }
    }
}
