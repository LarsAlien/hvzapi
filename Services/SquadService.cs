﻿using AutoMapper;
using HVZapi.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using HVZapi.Models.Domain_Model;
using HVZapi.Models.Data_Transfer_Objects;
using System.Security.Claims;
using HVZapi.Helpers;

namespace HVZapi.Services
{
    /// <summary>
    ///  SquadService class.
    /// Include all service for Squad table
    /// </summary>
    public class SquadService : ISquadService
    {
        private readonly GameDbContext _context;
        private readonly IMapper _mapper;

        public SquadService(GameDbContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        // Get all squads
        public async Task<IEnumerable<GetSquadDTO>> GetSquads()
        {
            return _mapper.Map<List<GetSquadDTO>>(await _context.Squads.ToListAsync());
        }

        // Get Squad by Id
        public async Task<GetSquadAndMembersDTO> GetSquad(int squadId, ClaimsPrincipal user)
        {

            await IsSquadMember(user, squadId);
            Squad squad = await _context.Squads
                            .Include(s => s.SquadMembers).ThenInclude(sm => sm.Player).ThenInclude(p => p.User)
                            .SingleOrDefaultAsync(s => s.Id == squadId);
            if (squad == null)
                throw new AppException($"Error: The squad you are trying to get with id: {squadId} is not exist");

            return _mapper.Map<GetSquadAndMembersDTO>(squad);
        }

        // Create new Squad
        public async Task<GetSquadDTO> CreateSquad(PostSquadDTO squadDTO, ClaimsPrincipal user)
        {
            List<Squad> squads = await _context.Squads
                .Include(s => s.SquadMembers)
                .ThenInclude(sm => sm.Player).ThenInclude(p => p.User)
                .Where(s => s.GameId == squadDTO.GameId)
                .ToListAsync();

            // Check if player is in squad
            var currentUserId = int.Parse(user.Identity.Name);
            bool isInSquad = squads.Any(s => s.SquadMembers.Any(sm => sm.Player.UserId == currentUserId));
            if ( !isInSquad && !user.IsInRole(Role.Admin))
                throw new AppException("User access was denied: Player already in squad");

            // Map, add, save and return new squad
            Squad squad = _mapper.Map<Squad>(squadDTO);
            _context.Squads.Add(squad);
            await _context.SaveChangesAsync();
            return _mapper.Map<GetSquadDTO>(squad);
        }

        // Adds new player to Squad
        public async Task<GetSquadMemberDTO> JoinSquad(PostSquadMemberDTO squadMemberDTO)
        {
            // Map, add, save and return new SquadMember
            SquadMember squadMember = _mapper.Map<SquadMember>(squadMemberDTO);
            squadMember.Rank = "Rook";
            _context.SquadMembers.Add(squadMember);
            await _context.SaveChangesAsync();
            return _mapper.Map<GetSquadMemberDTO>(squadMember);
        }

        public async void LeaveSquad(int squadId, ClaimsPrincipal user)
        {
            var currentUserId = int.Parse(user.Identity.Name);
            SquadMember squadMember = await _context.SquadMembers
                .Include(sm => sm.Player).ThenInclude(p => p.User).SingleAsync(sm => sm.Player.User.Id == currentUserId && sm.SquadId == squadId);
            if (squadMember == null) throw new AppException("The squadmember you are trying to access does not exist");
            _context.SquadMembers.Remove(squadMember);
            await _context.SaveChangesAsync();
        }

        // Update existing Squad
        public void UpdateSquad(GetSquadDTO squadDTO)
        {
            //Check if Squad exist
            bool find = _context.Squads.Any(c => c.Id == squadDTO.Id);

            if (!find)
                throw new AppException($"Error: Squad you are trying to update with id: {squadDTO.Id} is not found");

            // Map and save Squad
            Squad squad = _mapper.Map<Squad>(squadDTO);
            try
            {
                _context.Entry(squad).State = EntityState.Modified;
                 _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException ex)
            {
                Console.WriteLine(ex);

            }

        }

        // Delete Squad
        public async void DeleteSquad(int squadId)
        {
            // Check if Squad exists
            Squad squad = await _context.Squads.FindAsync(squadId);
            if (squad == null) throw new AppException("The squad you are trying to access does not exist");
            _context.Squads.Remove(squad);
            await _context.SaveChangesAsync();
        }

        // Get Squad Chats
        public async Task<List<GetChatDTO>> GetChat(int squadId, ClaimsPrincipal user)
        {
            //List of Chats from the User's squad
            List<Chat> chats = await _context.Chats
                            .Include(s => s.Player).ThenInclude(p => p.User)
                            .Where(s => s.SquadId == squadId)
                            .ToListAsync();

            SquadMember sm = await IsSquadMember(user, squadId);

            return _mapper.Map<List<GetChatDTO>>(chats);
        }

        // Post new Chat to Squad
        public async Task<GetChatDTO> PostChat(PostSquadChatDTO chatDTO, ClaimsPrincipal user)
        {
            SquadMember sm = await IsSquadMember(user, chatDTO.SquadId);

            Chat chat = _mapper.Map<Chat>(chatDTO);
            chat.ChatTime = DateTime.Now;
            _context.Chats.Add(chat);
            await _context.SaveChangesAsync();

            chat = await _context.Chats
                .Include(s => s.Player).ThenInclude(p => p.User).SingleAsync(c => c.Id == chat.Id);

            return _mapper.Map<GetChatDTO>(chat);
        }

        // Get Squad Checkin
        public async Task<List<GetCheckinDTO>> GetCheckin(int squadId, ClaimsPrincipal user)
        {
            SquadMember sm = await IsSquadMember(user, squadId);
            return _mapper.Map<List<GetCheckinDTO>>(await _context.SquadCheckIns.Where(c => c.SquadId == squadId).ToListAsync());
        }

        // Create new Squad Checkin 
        public async Task<GetCheckinDTO> PostCheckin(PostCheckinDTO checkinDTO, ClaimsPrincipal user)
        {
            SquadMember sm = await IsSquadMember(user, checkinDTO.SquadId);

            SquadCheckIn checkin = _mapper.Map<SquadCheckIn>(checkinDTO);
            _context.SquadCheckIns.Add(checkin);
            await _context.SaveChangesAsync();

            return _mapper.Map<GetCheckinDTO>(checkin);
        }

        // Check if user is Squad Member
        private async Task<SquadMember> IsSquadMember(ClaimsPrincipal user, int squadId)
        {
            // Chec if Squad exist
            Squad squad = await _context.Squads
                            .Include(s => s.SquadMembers).ThenInclude(sm => sm.Player).ThenInclude(p => p.User)
                            .SingleAsync(s => s.Id == squadId);
            if (squad == null)
                throw new AppException($"The squad you are tying to access with id: {squadId} doesn't exist ");

            // Set access to user and admin only
            var currentUserId = int.Parse(user.Identity.Name);
            // If it works, it works
            SquadMember sm = squad.SquadMembers.Count == 0
                ? null
                : squad.SquadMembers.First(sm => sm.Player.User.Id == currentUserId);
            if (sm?.Player.IsHuman == false && !user.IsInRole(Role.Admin))
                throw new NotAuthorizedException("User access was denied");

            return sm;
        }
    }
}
